@info "Importing packages"

using Pkg

# packages = ["CSV", "DataFrames", "DSP", "FFTW", "JLD2", "MLJ", "MLJDecisionTreeInterface", "Plots", "StatsBase"]
# Pkg.add(packages)

using CSV
using DataFrames
using DSP
using FFTW
using JLD2
using MLJ
using MLJDecisionTreeInterface
using Random
using Plots
using StatsBase

FFTW.set_num_threads(Sys.CPU_THREADS - 1)

m = Pkg.Operations.Context().env.manifest
println("       CSV $(m[findfirst(v -> v.name == "CSV", m)].version)")
println("DataFrames $(m[findfirst(v -> v.name == "DataFrames", m)].version)")
println("       DSP $(m[findfirst(v -> v.name == "DSP", m)].version)")
println("      FFTW $(m[findfirst(v -> v.name == "FFTW", m)].version)")
println("      JLD2 $(m[findfirst(v -> v.name == "JLD2", m)].version)")
println("       MLJ $(m[findfirst(v -> v.name == "MLJ", m)].version)")
println("     Plots $(m[findfirst(v -> v.name == "Plots", m)].version)")
println(" StatsBase $(m[findfirst(v -> v.name == "StatsBase", m)].version)")
println()

function generate_psd(epoch_n, acc_x, acc_y, acc_z, ang_x, ang_y, ang_z)
    fs = 50

    p = welch_pgram(acc_x[1, :], 4*fs, fs=fs)
    f = Vector(freq(p))

    acc_x_psd = zeros(epoch_n, length(power(p)))
    acc_y_psd = zeros(epoch_n, length(power(p)))
    acc_z_psd = zeros(epoch_n, length(power(p)))
    ang_x_psd = zeros(epoch_n, length(power(p)))
    ang_y_psd = zeros(epoch_n, length(power(p)))
    ang_z_psd = zeros(epoch_n, length(power(p)))

    for idx in 1:epoch_n
        acc_x_psd[idx, :] = power(welch_pgram(acc_x[idx, :], 4*fs, fs=fs, window=DSP.hanning))
        acc_y_psd[idx, :] = power(welch_pgram(acc_y[idx, :], 4*fs, fs=fs, window=DSP.hanning))
        acc_z_psd[idx, :] = power(welch_pgram(acc_z[idx, :], 4*fs, fs=fs, window=DSP.hanning))
        ang_x_psd[idx, :] = power(welch_pgram(ang_x[idx, :], 4*fs, fs=fs, window=DSP.hanning))
        ang_y_psd[idx, :] = power(welch_pgram(ang_y[idx, :], 4*fs, fs=fs, window=DSP.hanning))
        ang_z_psd[idx, :] = power(welch_pgram(ang_z[idx, :], 4*fs, fs=fs, window=DSP.hanning))
    end

    acc_x_psd = acc_x_psd[:, 5:end]
    acc_y_psd = acc_y_psd[:, 5:end]
    acc_z_psd = acc_z_psd[:, 5:end]
    ang_x_psd = ang_x_psd[:, 5:end]
    ang_y_psd = ang_y_psd[:, 5:end]
    ang_z_psd = ang_z_psd[:, 5:end]

    return acc_x_psd, acc_y_psd, acc_z_psd, ang_x_psd, ang_y_psd, ang_z_psd, f
end

@info "Testing regressor models"

# load subject data
@assert isfile("data/test_epochs_regressor.csv") "File data/test_epochs_regressor.csv cannot be opened!"
println("Loading: test_epochs_regressor.csv")
test_data = CSV.read("data/test_epochs_regressor.csv", header=false, DataFrame)

# load models
@assert isfile("models/regressor_model_sas.jlso") "File models/regressor_model_sas.jlso cannot be opened!"
println("Loading model: regressor_model_sas.jlso")
regressor_model_sas = machine("models/regressor_model_sas.jlso")
@assert isfile("models/regressor_model_aims8.jlso") "File models/regressor_model_aims8.jlso cannot be opened!"
println("Loading model: regressor_model_aims8.jlso")
regressor_model_aims8 = machine("models/regressor_model_aims8.jlso")
@assert isfile("models/regressor_model_aims9.jlso") "File models/regressor_model_aims9.jlso cannot be opened!"
println("Loading model: regressor_model_aims9.jlso")
regressor_model_aims9 = machine("models/regressor_model_aims9.jlso")
@assert isfile("models/regressor_model_aims10.jlso") "File models/regressor_model_aims10.jlso cannot be opened!"
println("Loading model: regressor_model_aims10.jlso")
regressor_model_aims10 = machine("models/regressor_model_aims10.jlso")
@assert isfile("models/regressor_model_aims.jlso") "File models/regressor_model_aims.jlso cannot be opened!"
println("Loading model: regressor_model_aims.jlso")
regressor_model_aims = machine("models/regressor_model_aims.jlso")
@assert isfile("models/regressor_scaler.jld") "File models/regressor_scaler.jld cannot be opened!"
println("Loading: regressor_scaler.jld")
regressor_scaler = JLD2.load_object("models/regressor_scaler.jld")

subject_id = convert.(Int64, test_data[:, 1])
no_subjects = unique(subject_id)
scores_all_epochs = test_data[:, 2:5]
x = Matrix(test_data[:, 6:end])
epoch_n = size(x, 1)
println("Number of subjects: $(length(no_subjects))")
println("Total number of epochs: $epoch_n")

# PSD
println("Processing: PSD")
acc_x = x[1:epoch_n, 1:500]
acc_y = x[1:epoch_n, 501:1000]
acc_z = x[1:epoch_n, 1001:1500]
ang_x = x[1:epoch_n, 1501:2000]
ang_y = x[1:epoch_n, 2001:2500]
ang_z = x[1:epoch_n, 2501:3000]

# acc_x_fft, acc_y_fft, acc_z_fft, ang_x_fft, ang_y_fft, ang_z_fft = generate_fft(epoch_n, acc_x, acc_y, acc_z, ang_x, ang_y, ang_z)
acc_x_psd, acc_y_psd, acc_z_psd, ang_x_psd, ang_y_psd, ang_z_psd = generate_psd(epoch_n, acc_x, acc_y, acc_z, ang_x, ang_y, ang_z)

# x = hcat(acc_x_fft, acc_y_fft, acc_z_fft, ang_x_fft, ang_y_fft, ang_z_fft)
x = hcat(acc_x_psd, acc_y_psd, acc_z_psd, ang_x_psd, ang_y_psd, ang_z_psd)

# standardize
println("Processing: standardize")
x = StatsBase.transform(regressor_scaler, x)
println()

first_epoch_idx = zeros(Int64, no_subjects[end])
for idx in 1:no_subjects[end]
    first_epoch_idx[idx] = findfirst(isequal(idx), subject_id)
end

data_full = DataFrame(x, :auto)
data_first_epoch = DataFrame(x[first_epoch_idx, :], :auto)
scores_first_epoch = scores_all_epochs[first_epoch_idx, :]

println("Calculating predictions: SAS, all available epochs")
sas = scores_all_epochs[:, 1]
sas_all = round.(MLJ.predict(regressor_model_sas, data_full), digits=0)
p1 = Plots.scatter(sas .- sas_all, ylims=(-8, +8), xlabel="all epochs", ylabel="SAS error", ms=1, mc=:black, label=false)
println("RandomForestRegressor accuracy report:")
m = RSquared()
println("\tR²: ", round(m(sas_all, sas), digits=4))
m = RootMeanSquaredError()
println("\tRMSE: ", round(m(sas_all, sas), digits=4))
println()

println("Calculating predictions: AIMS8, all available epochs")
aims8 = scores_all_epochs[:, 2]
aims8_all = round.(MLJ.predict(regressor_model_aims8, data_full), digits=0)
p2 = Plots.scatter(aims8 .- aims8_all, ylims=(-8, +8), xlabel="all epochs", ylabel="AIMS8 error", ms=1, mc=:black, label=false)
println("RandomForestRegressor accuracy report:")
m = RSquared()
println("\tR²: ", round(m(aims8_all, aims8), digits=4))
m = RootMeanSquaredError()
println("\tRMSE: ", round(m(aims8_all, aims8), digits=4))
println()

println("Calculating predictions: AIMS9, all available epochs")
aims9 = scores_all_epochs[:, 3]
aims9_all = round.(MLJ.predict(regressor_model_aims9, data_full), digits=0)
p3 = Plots.scatter(aims9 .- aims9_all, ylims=(-8, +8), xlabel="all epochs", ylabel="AIMS9 error", ms=1, mc=:black, label=false)
println("RandomForestRegressor accuracy report:")
m = RSquared()
println("\tR²: ", round(m(aims9_all, aims9), digits=4))
m = RootMeanSquaredError()
println("\tRMSE: ", round(m(aims9_all, aims9), digits=4))
println()

println("Calculating predictions: AIMS10, all available epochs")
aims10 = scores_all_epochs[:, 4]
aims10_all = round.(MLJ.predict(regressor_model_aims10, data_full), digits=0)
p4 = Plots.scatter(aims10 .- aims10_all, ylims=(-8, +8), xlabel="all epochs", ylabel="AIMS10 error", ms=1, mc=:black, label=false)
println("RandomForestRegressor accuracy report:")
m = RSquared()
println("\tR²: ", round(m(aims10_all, aims10), digits=4))
m = RootMeanSquaredError()
println("\tRMSE: ", round(m(aims10_all, aims10), digits=4))
println()

println("Calculating predictions: AIMS, all available epochs")
aims = scores_all_epochs[:, 2:4]
aims = sum.(eachrow(aims[:, names(aims, Real)]))
aims_all = round.(MLJ.predict(regressor_model_aims, data_full), digits=0)
p5 = Plots.scatter(aims .- aims_all, ylims=(-8, +8), xlabel="all epochs", ylabel="AIMS error", ms=1, mc=:black, label=false)
println("RandomForestRegressor accuracy report:")
m = RSquared()
println("\tR²: ", round(m(aims_all, aims), digits=4))
m = RootMeanSquaredError()
println("\tRMSE: ", round(m(aims_all, aims), digits=4))
println()

p_aims = Plots.plot(p2, p3, p4, p5, layout=(2, 2))
p = Plots.plot(p1, p_aims, layout=(2, 1), xlabelfontsize=6, ylabelfontsize=6, xtickfontsize=4, ytickfontsize=4, legendfontsize=4)
savefig(p, "reports/test_regressor_all.png")

println("Calculating predictions: SAS, first epoch only")
sas = scores_first_epoch[:, 1]
sas_first = round.(MLJ.predict(regressor_model_sas, data_first_epoch), digits=0)
p1 = Plots.scatter(sas .- sas_first, ylims=(-8, +8), xlabel="first epochs", ylabel="SAS error", ms=1, mc=:black, label=false)
println("RandomForestRegressor accuracy report:")
m = RSquared()
println("\tR²: ", round(m(sas_first, sas), digits=4))
m = RootMeanSquaredError()
println("\tRMSE: ", round(m(sas_first, sas), digits=4))
println()

println("Calculating predictions: AIMS8, first epoch only")
aims8 = scores_first_epoch[:, 2]
aims8_first = round.(MLJ.predict(regressor_model_aims8, data_first_epoch), digits=0)
p2 = Plots.scatter(aims8 .- aims8_first, ylims=(-8, +8), xlabel="first epochs", ylabel="AIMS8 error", ms=1, mc=:black, label=false)
println("RandomForestRegressor accuracy report:")
m = RSquared()
println("\tR²: ", round(m(aims8_first, aims8), digits=4))
m = RootMeanSquaredError()
println("\tRMSE: ", round(m(aims8_first, aims8), digits=4))
println()

println("Calculating predictions: AIMS9, first epoch only")
aims9 = scores_first_epoch[:, 3]
aims9_first = round.(MLJ.predict(regressor_model_aims9, data_first_epoch), digits=0)
p3 = Plots.scatter(aims9 .- aims9_first, ylims=(-8, +8), xlabel="first epochs", ylabel="AIMS9 error", ms=1, mc=:black, label=false)
println("RandomForestRegressor accuracy report:")
m = RSquared()
println("\tR²: ", round(m(aims9_first, aims9), digits=4))
m = RootMeanSquaredError()
println("\tRMSE: ", round(m(aims9_first, aims9), digits=4))
println()

println("Calculating predictions: AIMS10, first epoch only")
aims10 = scores_first_epoch[:, 4]
aims10_first = round.(MLJ.predict(regressor_model_aims10, data_first_epoch), digits=0)
p4 = Plots.scatter(aims10 .- aims10_first, ylims=(-8, +8), xlabel="first epochs", ylabel="AIMS10 error", ms=1, mc=:black, label=false)
println("RandomForestRegressor accuracy report:")
m = RSquared()
println("\tR²: ", round(m(aims10_first, aims10), digits=4))
m = RootMeanSquaredError()
println("\tRMSE: ", round(m(aims10_first, aims10), digits=4))
println()

println("Calculating predictions: AIMS, first epoch only")
aims = scores_first_epoch[:, 2:4]
aims = sum.(eachrow(aims[:, names(aims, Real)]))
aims_first = round.(MLJ.predict(regressor_model_aims, data_first_epoch), digits=0)
p5 = Plots.scatter(aims .- aims_first, ylims=(-8, +8), xlabel="first epochs", ylabel="AIMS error", ms=1, mc=:black, label=false)
println("RandomForestRegressor accuracy report:")
m = RSquared()
println("\tR²: ", round(m(aims_first, aims), digits=4))
m = RootMeanSquaredError()
println("\tRMSE: ", round(m(aims_first, aims), digits=4))
println()

p_aims = Plots.plot(p2, p3, p4, p5, layout=(2, 2))
p = Plots.plot(p1, p_aims, layout=(2, 1), xlabelfontsize=6, ylabelfontsize=6, xtickfontsize=4, ytickfontsize=4, legendfontsize=4)

savefig(p, "reports/test_regressor_first.png")

println()

@info "Testing classifier models"

# load subject data
if isfile("data/test_epochs.csv")
    println("Loading: test_epochs.csv")
    test_data = CSV.read("data/test_epochs.csv", header=false, DataFrame)
else
    error("File data/test_epochs.csv cannot be opened!")
    exit(-1)
end

# load models
if isfile("models/classifier_model.jlso")
    println("Loading model: classifier_model.jlso")
    model_rfc = machine("models/classifier_model.jlso")
else
    error("File models/classifier_model.jlso cannot be opened!")
    exit(-1)
end
if isfile("models/classifier_scaler.jld")
    println("Loading: classifier_scaler.jld")
    classifier_scaler = JLD2.load_object("models/classifier_scaler.jld")
else
    error("File models/classifier_scaler.jld cannot be opened!")
    exit(-1)
end

subject_id = convert.(Int64, test_data[:, 1])
no_subjects = unique(subject_id)
epoch_type = string.(test_data[:, 2])
replace!(epoch_type, "0.0" => "no tremor")
replace!(epoch_type, "1.0" => "tremor")
x = Matrix(test_data[:, 3:end])
epoch_n = size(x, 1)
println("Number of subjects: $(length(no_subjects))")
println("Total number of epochs: $epoch_n")

# PSD
println("Processing: PSD")
acc_x = x[1:epoch_n, 1:500]
acc_y = x[1:epoch_n, 501:1000]
acc_z = x[1:epoch_n, 1001:1500]
ang_x = x[1:epoch_n, 1501:2000]
ang_y = x[1:epoch_n, 2001:2500]
ang_z = x[1:epoch_n, 2501:3000]

acc_x_psd, acc_y_psd, acc_z_psd, ang_x_psd, ang_y_psd, ang_z_psd = generate_psd(epoch_n, acc_x, acc_y, acc_z, ang_x, ang_y, ang_z)

x = hcat(acc_x_psd, acc_y_psd, acc_z_psd, ang_x_psd, ang_y_psd, ang_z_psd)

# standardize
println("Processing: standardize")
x = StatsBase.transform(classifier_scaler, x)
println()

first_epoch_idx = zeros(Int64, no_subjects[end])
for idx in 1:no_subjects[end]
    first_epoch_idx[idx] = findfirst(isequal(idx), subject_id)
end

data_full = DataFrame(x, :auto)
data_first_epoch = DataFrame(x[first_epoch_idx, :], :auto)

println("Calculating predictions: classifier, all available epochs")
yhat = MLJ.predict(model_rfc, data_full)
yhat_sas = round.(MLJ.predict(regressor_model_sas, data_full))
yhat_aims8 = round.(MLJ.predict(regressor_model_aims8, data_full))
yhat_aims9 = round.(MLJ.predict(regressor_model_aims9, data_full))
yhat_aims10 = round.(MLJ.predict(regressor_model_aims10, data_full))
yhat_aims = round.(MLJ.predict(regressor_model_aims, data_full))
subjects_allepochs = zeros(Int64, 88)
subjects_allepochs_adj = zeros(Int64, 88)
subjects_firstepoch = zeros(Int64, 88)
subjects_firstepoch_adj = zeros(Int64, 88)

for idx in 1:length(no_subjects)

    s = lpad(string(idx), 3)
    print("subject: $s [epochs: $(lpad(string(size(subject_id[subject_id .== idx])[1]), 3))] ")
    if unique(epoch_type[subject_id .== idx])[1] == "no tremor"
        print("group: NO TREMOR, ")
    else
        print("group:    TREMOR, ")
    end

    p_tremor = broadcast(pdf, yhat[subject_id .== idx], "tremor") |> mean
    p_notremor = broadcast(pdf, yhat[subject_id .== idx], "no tremor") |> mean
    p_tremor_adj = p_tremor
    p_notremor_adj = p_notremor

    if mean(yhat_sas[subject_id .== idx]) < 1.0
        p_tremor_adj -= 0.1
        p_notremor_adj += 0.1
    elseif mean(yhat_sas[subject_id .== idx]) >= 1.0 && mean(yhat_sas[subject_id .== idx]) < 2.0
        p_tremor_adj += 0.1
        p_notremor_adj -= 0.1
    elseif mean(yhat_sas[subject_id .== idx]) >= 2.0 && mean(yhat_sas[subject_id .== idx]) < 3.0
        p_tremor_adj += 0.2
        p_notremor_adj -= 0.2
    elseif mean(yhat_sas[subject_id .== idx]) >= 3.0
        p_tremor_adj += 0.3
        p_notremor_adj -= 0.3
    end

    if mean(yhat_aims8[subject_id .== idx]) < 1.0
        p_tremor_adj -= 0.1
        p_notremor_adj += 0.1
    elseif mean(yhat_aims8[subject_id .== idx]) >= 1.0 && mean(yhat_aims8[subject_id .== idx]) < 2.0
        p_tremor_adj += 0.1
        p_notremor_adj -= 0.1
    elseif mean(yhat_aims8[subject_id .== idx]) >= 2.0 && mean(yhat_aims8[subject_id .== idx]) < 3.0
        p_tremor_adj += 0.2
        p_notremor_adj -= 0.2
    elseif mean(yhat_aims8[subject_id .== idx]) >= 3.0
        p_tremor_adj += 0.3
        p_notremor_adj -= 0.3
    end

    if mean(yhat_aims9[subject_id .== idx]) < 1.0
        p_tremor_adj -= 0.1
        p_notremor_adj += 0.1
    elseif mean(yhat_aims9[subject_id .== idx]) >= 1.0 && mean(yhat_aims9[subject_id .== idx]) < 2.0
        p_tremor_adj += 0.1
        p_notremor_adj -= 0.1
    elseif mean(yhat_aims9[subject_id .== idx]) >= 2.0 && mean(yhat_aims9[subject_id .== idx]) < 3.0
        p_tremor_adj += 0.2
        p_notremor_adj -= 0.2
    elseif mean(yhat_aims9[subject_id .== idx]) >= 3.0
        p_tremor_adj += 0.3
        p_notremor_adj -= 0.3
    end

    p_tremor_adj > 1.0 && (p_tremor_adj = 1.0)
    p_tremor_adj < 0.0 && (p_tremor_adj = 0.0)
    p_notremor_adj > 1.0 && (p_notremor_adj = 1.0)
    p_notremor_adj < 0.0 && (p_notremor_adj = 0.0)

    if p_notremor > p_tremor
        print("prediction: NO TREMOR, p = $(rpad(string(round(p_notremor, digits=2)), 4, '0')); ")
    else
        print("prediction:    TREMOR, p = $(rpad(string(round(p_tremor, digits=2)), 4, '0')); ")
        subjects_allepochs[idx] = 1
    end

    if p_notremor_adj > p_tremor_adj
        println("adj. prediction: NO TREMOR, p = $(rpad(string(round(p_notremor_adj, digits=2)), 4, '0'))")
    else
        println("adj. prediction:    TREMOR, p = $(rpad(string(round(p_tremor_adj, digits=2)), 4, '0'))")
        subjects_allepochs_adj[idx] = 1
    end

end

println()

y = epoch_type
println("RFC model accuracy report:")
println("\tlog_loss: ", round(log_loss(yhat, y) |> mean, digits=4))
println("\tAUC: ", round(auc(yhat, y), digits=4))
println("\tmisclassification rate: ", round(misclassification_rate(mode.(yhat), y), digits=2))
println("\taccuracy: ", round(1 - misclassification_rate(mode.(yhat), y), digits=2))
println("confusion matrix:")
groups = epoch_type[first_epoch_idx]
cm = zeros(Int64, 2, 2)
cm[1, 1] = count(subjects_allepochs .== 0 .&& groups .== "no tremor")
cm[1, 2] = count(subjects_allepochs .== 0 .&& groups .== "tremor")
cm[2, 1] = count(subjects_allepochs .== 1 .&& groups .== "no tremor")
cm[2, 2] = count(subjects_allepochs .== 1 .&& groups .== "tremor")
# cm = confusion_matrix(mode.(yhat), y)
println("\tsensitivity (TPR): ", round(cm[1, 1] / sum(cm[:, 1]), digits=2))
println("\tspecificity (TNR): ", round(cm[2, 2] / sum(cm[:, 2]), digits=2))
println("""
             ┌───────────────────┐
             │       Group       │
┌────────────┼─────────┬─────────┤
│ Prediction │no tremor│ tremor  │
├────────────┼─────────┼─────────┤
│ no tremor  │ $(rpad(cm[1, 1], 4, " "))    │ $(rpad(cm[1, 2], 4, " "))    │
├────────────┼─────────┼─────────┤
│   tremor   │ $(rpad(cm[2, 1], 4, " "))    │ $(rpad(cm[2, 2], 4, " "))    │
└────────────┴─────────┴─────────┘""")

println()
println("adjusted prediction:")
cm_adj = zeros(Int64, 2, 2)
cm_adj[1, 1] = count(subjects_allepochs_adj .== 0 .&& groups .== "no tremor")
cm_adj[1, 2] = count(subjects_allepochs_adj .== 0 .&& groups .== "tremor")
cm_adj[2, 1] = count(subjects_allepochs_adj .== 1 .&& groups .== "no tremor")
cm_adj[2, 2] = count(subjects_allepochs_adj .== 1 .&& groups .== "tremor")
println("\tsensitivity (TPR): ", round(cm_adj[1, 1] / sum(cm_adj[:, 1]), digits=2))
println("\tspecificity (TNR): ", round(cm_adj[2, 2] / sum(cm_adj[:, 2]), digits=2))
println("""
             ┌───────────────────┐
             │       Group       │
┌────────────┼─────────┬─────────┤
│ Prediction │no tremor│ tremor  │
├────────────┼─────────┼─────────┤
│ no tremor  │ $(rpad(cm_adj[1, 1], 4, " "))    │ $(rpad(cm_adj[1, 2], 4, " "))    │
├────────────┼─────────┼─────────┤
│   tremor   │ $(rpad(cm_adj[2, 1], 4, " "))    │ $(rpad(cm_adj[2, 2], 4, " "))    │
└────────────┴─────────┴─────────┘""")
println()

println("Calculating predictions: first epoch only")
yhat = MLJ.predict(model_rfc, data_first_epoch)
yhat_sas = round.(MLJ.predict(regressor_model_sas, data_first_epoch))
yhat_aims8 = round.(MLJ.predict(regressor_model_aims8, data_first_epoch))
yhat_aims9 = round.(MLJ.predict(regressor_model_aims9, data_first_epoch))
yhat_aims10 = round.(MLJ.predict(regressor_model_aims10, data_first_epoch))
yhat_aims = round.(MLJ.predict(regressor_model_aims, data_first_epoch))

for idx in 1:length(no_subjects)

    s = lpad(string(idx), 3)
    print("subject: $s ")
    if unique(epoch_type[subject_id .== idx])[1] == "no tremor"
        print("group: NO TREMOR, ")
    else
        print("group:    TREMOR, ")
    end

    p_tremor = broadcast(pdf, yhat[idx], "tremor") |> mean
    p_notremor = broadcast(pdf, yhat[idx], "no tremor") |> mean
    p_tremor_adj = p_tremor
    p_notremor_adj = p_notremor

    if yhat_sas[idx] < 1.0
        p_notremor_adj += 0.1
        p_tremor_adj -= 0.1
    elseif yhat_sas[idx] in 1.0:2.0
        p_tremor_adj += 0.1
        p_notremor_adj -= 0.1
    elseif yhat_sas[idx] in 2.0:3.0
        p_tremor_adj += 0.2
        p_notremor_adj -= 0.2
    elseif yhat_sas[idx] > 3.0
        p_tremor_adj += 0.3
        p_notremor_adj -= 0.3
    end

    if yhat_aims8[idx] < 1.0
        p_notremor_adj += 0.1
        p_tremor_adj -= 0.1
    elseif yhat_aims8[idx] in 1.0:2.0
        p_tremor_adj += 0.1
        p_notremor_adj -= 0.1
    elseif yhat_aims8[idx] in 2.0:3.0
        p_tremor_adj += 0.2
        p_notremor_adj -= 0.2
    elseif yhat_aims8[idx] > 3.0
        p_tremor_adj += 0.3
        p_notremor_adj -= 0.3
    end

    if yhat_aims9[idx] < 1.0
        p_notremor_adj += 0.1
        p_tremor_adj -= 0.1
    elseif yhat_aims9[idx] in 1.0:2.0
        p_tremor_adj += 0.1
        p_notremor_adj -= 0.1
    elseif yhat_aims9[idx] in 2.0:3.0
        p_tremor_adj += 0.2
        p_notremor_adj -= 0.2
    elseif yhat_aims9[idx] > 3.0
        p_tremor_adj += 0.3
        p_notremor_adj -= 0.3
    end

    p_tremor_adj > 1.0 && (p_tremor_adj = 1.0)
    p_tremor_adj < 0.0 && (p_tremor_adj = 0.0)
    p_notremor_adj > 1.0 && (p_notremor_adj = 1.0)
    p_notremor_adj < 0.0 && (p_notremor_adj = 0.0)

    if p_notremor > p_tremor
        print("prediction: NO TREMOR, p = $(rpad(string(round(p_notremor, digits=2)), 4, '0')); ")
    else
        print("prediction:    TREMOR, p = $(rpad(string(round(p_tremor, digits=2)), 4, '0')); ")
        subjects_firstepoch[idx] = 1
    end

    if p_notremor_adj > p_tremor_adj
        println("adj. prediction: NO TREMOR, p = $(rpad(string(round(p_notremor_adj, digits=2)), 4, '0'))")
    else
        println("adj. prediction:    TREMOR, p = $(rpad(string(round(p_tremor_adj, digits=2)), 4, '0'))")
        subjects_firstepoch_adj[idx] = 1
    end

end

println()

y = epoch_type[first_epoch_idx]
println("RFC model accuracy report:")
println("\tlog_loss: ", round(log_loss(yhat, y) |> mean, digits=4))
println("\tAUC: ", round(auc(yhat, y), digits=4))
println("\tmisclassification rate: ", round(misclassification_rate(mode.(yhat), y), digits=2))
println("\taccuracy: ", 1 - round(misclassification_rate(mode.(yhat), y), digits=2))
println("confusion matrix:")
# cm = confusion_matrix(mode.(yhat), y)
cm = zeros(Int64, 2, 2)
cm[1, 1] = count(subjects_firstepoch .== 0 .&& groups .== "no tremor")
cm[1, 2] = count(subjects_firstepoch .== 0 .&& groups .== "tremor")
cm[2, 1] = count(subjects_firstepoch .== 1 .&& groups .== "no tremor")
cm[2, 2] = count(subjects_firstepoch .== 1 .&& groups .== "tremor")
println("\tsensitivity (TPR): ", round(cm[1, 1] / sum(cm[:, 1]), digits=2))
println("\tspecificity (TNR): ", round(cm[2, 2] / sum(cm[:, 2]), digits=2))
println("""
             ┌───────────────────┐
             │       Group       │
┌────────────┼─────────┬─────────┤
│ Prediction │no tremor│ tremor  │
├────────────┼─────────┼─────────┤
│ no tremor  │ $(rpad(cm[1, 1], 4, " "))    │ $(rpad(cm[1, 2], 4, " "))    │
├────────────┼─────────┼─────────┤
│   tremor   │ $(rpad(cm[2, 1], 4, " "))    │ $(rpad(cm[2, 2], 4, " "))    │
└────────────┴─────────┴─────────┘""")

println()
println("adjusted prediction:")
cm_adj = zeros(Int64, 2, 2)
cm_adj[1, 1] = count(subjects_allepochs_adj .== 0 .&& groups .== "no tremor")
cm_adj[1, 2] = count(subjects_allepochs_adj .== 0 .&& groups .== "tremor")
cm_adj[2, 1] = count(subjects_allepochs_adj .== 1 .&& groups .== "no tremor")
cm_adj[2, 2] = count(subjects_allepochs_adj .== 1 .&& groups .== "tremor")
println("\tsensitivity (TPR): ", round(cm_adj[1, 1] / sum(cm_adj[:, 1]), digits=2))
println("\tspecificity (TNR): ", round(cm_adj[2, 2] / sum(cm_adj[:, 2]), digits=2))
println("""
             ┌───────────────────┐
             │       Group       │
┌────────────┼─────────┬─────────┤
│ Prediction │no tremor│ tremor  │
├────────────┼─────────┼─────────┤
│ no tremor  │ $(rpad(cm_adj[1, 1], 4, " "))    │ $(rpad(cm_adj[1, 2], 4, " "))    │
├────────────┼─────────┼─────────┤
│   tremor   │ $(rpad(cm_adj[2, 1], 4, " "))    │ $(rpad(cm_adj[2, 2], 4, " "))    │
└────────────┴─────────┴─────────┘""")
