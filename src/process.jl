export process

function create_epochs(path::String, file::String, subject_id::String)

    input_file = path * "/" * file
    @mput input_file
    mat"""load(input_file)"""

    # sampling frequency 50 Hz
    fs = 50.0

    # 1 epoch = 10 seconds = 500 samples
    epoch_time = collect(0:1/fs:10)
    epoch_time = epoch_time[1:end - 1]'

    file_fs = round(1 / mat"""second(Acceleration.Timestamp(2)) - second(Acceleration.Timestamp(1))""")
    if file_fs == fs
        mat"""acc_x = Acceleration.X"""
        mat"""acc_y = Acceleration.Y"""
        mat"""acc_z = Acceleration.Z"""
        @mget acc_x
        @mget acc_y
        @mget acc_z
        mat"""ang_x = AngularVelocity.X"""
        mat"""ang_y = AngularVelocity.Y"""
        mat"""ang_z = AngularVelocity.Z"""
        @mget ang_x
        @mget ang_y
        @mget ang_z
    else
        mat"""acc_x = resample(Acceleration.X, $fs, $file_fs)"""
        mat"""acc_y = resample(Acceleration.Y, $fs, $file_fs)"""
        mat"""acc_z = resample(Acceleration.Z, $fs, $file_fs)"""
        @mget acc_x
        @mget acc_y
        @mget acc_z
        mat"""ang_x = resample(AngularVelocity.X, $fs, $file_fs)"""
        mat"""ang_y = resample(AngularVelocity.Y, $fs, $file_fs)"""
        mat"""ang_z = resample(AngularVelocity.Z, $fs, $file_fs)"""
        @mget ang_x
        @mget ang_y
        @mget ang_z
    end

    # ignore: mat"""ori_x = Orientation.X"""
    # ignore: mat"""ori_y = Orientation.Y"""
    # ignore: mat"""ori_z = Orientation.Z"""
    # ignore: @mget ori_x
    # ignore: @mget ori_y
    # ignore: @mget ori_z

    # trim 1 sec (50 samples) at the beginning and the end
    fs = Int64(fs)
    acc_x = acc_x[fs:(end - fs)]
    acc_y = acc_y[fs:(end - fs)]
    acc_z = acc_z[fs:(end - fs)]
    ang_x = ang_x[fs:(end - fs)]
    ang_y = ang_y[fs:(end - fs)]
    ang_z = ang_z[fs:(end - fs)]
    # ignore: ori_x = ori_x[fs:end-fs]
    # ignore: ori_y = ori_y[fs:(end - fs)]
    # ignore: ori_z = ori_z[fs:(end - fs)]

    # how many 10-sec epochs
    epoch_n = Int(floor(length(acc_x)/500))

    # create and save epochs 
    epoch_m = zeros(epoch_n, 3002)
    for idx = 1:epoch_n
        accx = (acc_x[(((idx - 1) * 500) + 1):(idx * 500)])'
        accy = (acc_y[(((idx - 1) * 500) + 1):(idx * 500)])'
        accz = (acc_z[(((idx - 1) * 500) + 1):(idx * 500)])'
        angx = (ang_x[(((idx - 1) * 500) + 1):(idx * 500)])'
        angy = (ang_y[(((idx - 1) * 500) + 1):(idx * 500)])'
        angz = (ang_z[(((idx - 1) * 500) + 1):(idx * 500)])'
        # ignore: oriy = (ori_y[(((idx - 1) * 500) + 1):(idx * 500)])'
        # ignore: oriz = (ori_z[(((idx - 1) * 500) + 1):(idx * 500)])'
        epoch_type = occursin("notremor", file) ? 0 : 1
        epoch_m[idx, :] = hcat(subject_id, epoch_type, accx, accy, accz, angx, angy, angz)
    end

    file_out = "data/predict_epochs.csv"
    CSV.write(file_out, Tables.table(epoch_m), append=true)

    return epoch_n

end

function create_epochs_regressor(path::String, file::String, subject_id::String)

    input_file = path * "/" * file
    @mput input_file
    mat"""load(input_file)"""

    # sampling frequency 50 Hz
    fs = 50.0

    # 1 epoch = 10 seconds = 500 samples
    epoch_time = collect(0:1/fs:10)
    epoch_time = epoch_time[1:end - 1]'

    file_fs = round(1 / mat"""second(Acceleration.Timestamp(2)) - second(Acceleration.Timestamp(1))""")
    if file_fs == 50
        mat"""acc_x = Acceleration.X"""
        mat"""acc_y = Acceleration.Y"""
        mat"""acc_z = Acceleration.Z"""
        @mget acc_x
        @mget acc_y
        @mget acc_z
        mat"""ang_x = AngularVelocity.X"""
        mat"""ang_y = AngularVelocity.Y"""
        mat"""ang_z = AngularVelocity.Z"""
        @mget ang_x
        @mget ang_y
        @mget ang_z
    else
        mat"""acc_x = resample(Acceleration.X, $fs, $file_fs)"""
        mat"""acc_y = resample(Acceleration.Y, $fs, $file_fs)"""
        mat"""acc_z = resample(Acceleration.Z, $fs, $file_fs)"""
        @mget acc_x
        @mget acc_y
        @mget acc_z
        mat"""ang_x = resample(AngularVelocity.X, $fs, $file_fs)"""
        mat"""ang_y = resample(AngularVelocity.Y, $fs, $file_fs)"""
        mat"""ang_z = resample(AngularVelocity.Z, $fs, $file_fs)"""
        @mget ang_x
        @mget ang_y
        @mget ang_z
    end

    # ignore: mat"""ori_x = Orientation.X"""
    # ignore: mat"""ori_y = Orientation.Y"""
    # ignore: mat"""ori_z = Orientation.Z"""
    # ignore: @mget ori_x
    # ignore: @mget ori_y
    # ignore: @mget ori_z

    # trim 1 sec (50 samples) at the beginning and the end
    fs = Int64(fs)
    acc_x = acc_x[fs:(end - fs)]
    acc_y = acc_y[fs:(end - fs)]
    acc_z = acc_z[fs:(end - fs)]
    ang_x = ang_x[fs:(end - fs)]
    ang_y = ang_y[fs:(end - fs)]
    ang_z = ang_z[fs:(end - fs)]
    # ignore: ori_x = ori_x[fs:end-fs]
    # ignore: ori_y = ori_y[fs:(end - fs)]
    # ignore: ori_z = ori_z[fs:(end - fs)]

    # how many 10-sec epochs
    epoch_n = Int(floor(length(acc_x)/500))

    # create and save epochs 
    epoch_m = zeros(epoch_n, 3001)
    for idx = 1:epoch_n
        accx = (acc_x[(((idx - 1) * 500) + 1):(idx * 500)])'
        accy = (acc_y[(((idx - 1) * 500) + 1):(idx * 500)])'
        accz = (acc_z[(((idx - 1) * 500) + 1):(idx * 500)])'
        angx = (ang_x[(((idx - 1) * 500) + 1):(idx * 500)])'
        angy = (ang_y[(((idx - 1) * 500) + 1):(idx * 500)])'
        angz = (ang_z[(((idx - 1) * 500) + 1):(idx * 500)])'
        # ignore: oriy = (ori_y[(((idx - 1) * 500) + 1):(idx * 500)])'
        # ignore: oriz = (ori_z[(((idx - 1) * 500) + 1):(idx * 500)])'
        epoch_m[idx, :] = hcat(subject_id, accx, accy, accz, angx, angy, angz)
    end

    file_out = "data/predict_epochs_regressor.csv"
    CSV.write(file_out, Tables.table(epoch_m), append=true)

    return epoch_n

end

function process(data_path::String)

    @assert isdir(data_path) "The directory: $data_path does not exist"
    data_path = expanduser(data_path)

    @assert isdir("data") "Folder data does not exist in the current path."
    isfile("data/predict_epochs.csv") && rm("data/predict_epochs.csv")

    @info "Starting MATLAB and processing data files.."

    files = readdir(data_path)
    for idx in eachindex(files)
        print("Processing file: $(files[idx]) ")
        epoch_n = create_epochs(data_path, files[idx], idx)
        println("epochs created: $epoch_n")
    end

    @assert isdir("data") "Folder data does not exist in the current path."
    isfile("data/predict_epochs_regressor.csv") && rm("data/predict_epochs_regressor.csv")

    df = CSV.read("data/test.csv", DataFrame)
    files = df[:, 1]
    for idx in eachindex(files)
        print("Processing file: $(files[idx]) ")
        epoch_n = create_epochs_regressor(data_path, files[idx], idx)
        println("epochs created: $epoch_n")
    end

    data_tmp = CSV.read("data/predict_epochs_regressor.csv", DataFrame, header=false)
    id = collect(1:nrow(df))
    scores = zeros(Int64, nrow(data_tmp), 4)
    for idx1 in eachindex(id)
        for idx2 in 1:nrow(data_tmp)
            if data_tmp[idx2, 1] == id[idx1]
                scores[idx2, :] = Vector(df[idx1, 2:5])
            end
        end
    end

    data = hcat(Int64.(data_tmp[:, 1]), scores)
    data = hcat(data, Matrix(data_tmp[:, 2:end]))
    df = DataFrame(data, :auto)
    file_out = "data/predict_epochs_regressor.csv"
    CSV.write(file_out, df, header=false)

    return nothing

end
