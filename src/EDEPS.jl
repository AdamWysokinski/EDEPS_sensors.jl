"""
Julia toolbox that analyses iPad/iPhone gyroscope data using RandomForestClassifier and RandomForestRegressor models to detect extrapyramidal hand tremor.
"""
module EDEPS

@info "Importing packages"

using Pkg
using CSV
using DataFrames
using DSP
using FFTW
using JLD2
using MATLAB
using MLJ
using MLJDecisionTreeInterface
using Plots
using Random
using StatsBase

FFTW.set_num_threads(Sys.CPU_THREADS - 1)

m = Pkg.Operations.Context().env.manifest
println("       CSV $(m[findfirst(v -> v.name == "CSV", m)].version)")
println("DataFrames $(m[findfirst(v -> v.name == "DataFrames", m)].version)")
println("       DSP $(m[findfirst(v -> v.name == "DSP", m)].version)")
println("      FFTW $(m[findfirst(v -> v.name == "FFTW", m)].version)")
println("      JLD2 $(m[findfirst(v -> v.name == "JLD2", m)].version)")
println("    MATLAB $(m[findfirst(v -> v.name == "MATLAB", m)].version)")
println("       MLJ $(m[findfirst(v -> v.name == "MLJ", m)].version)")
println(" MLJ (DTI) $(m[findfirst(v -> v.name == "MLJDecisionTreeInterface", m)].version) (MLJDecisionTreeInterface)")
println("     Plots $(m[findfirst(v -> v.name == "Plots", m)].version)")
println(" StatsBase $(m[findfirst(v -> v.name == "StatsBase", m)].version)")
println()

@info "Loading models"

@assert isfile("models/regressor_model_sas.jlso") "File models/regressor_model_sas.jlso cannot be opened!"
println("Loading model: regressor_model_sas.jlso")
regressor_model_sas = machine("models/regressor_model_sas.jlso")

@assert isfile("models/regressor_model_aims8.jlso") "File models/regressor_model_aims8.jlso cannot be opened!"
println("Loading model: regressor_model_aims8.jlso")
regressor_model_aims8 = machine("models/regressor_model_aims8.jlso")

@assert isfile("models/regressor_model_aims9.jlso") "File models/regressor_model_aims9.jlso cannot be opened!"
println("Loading model: regressor_model_aims9.jlso")
regressor_model_aims9 = machine("models/regressor_model_aims9.jlso")

@assert isfile("models/regressor_model_aims10.jlso") "File models/regressor_model_aims10.jlso cannot be opened!"
println("Loading model: regressor_model_aims10.jlso")
regressor_model_aims10 = machine("models/regressor_model_aims10.jlso")

@assert isfile("models/regressor_model_aims.jlso") "File models/regressor_model_aims.jlso cannot be opened!"
println("Loading model: regressor_model_aims.jlso")
regressor_model_aims = machine("models/regressor_model_aims.jlso")

@assert isfile("models/regressor_scaler.jld") "File models/regressor_scaler.jld cannot be opened!"
println("Loading: regressor_scaler.jld")
regressor_scaler = JLD2.load_object("models/regressor_scaler.jld")

include("process.jl")
include("predict.jl")

end # EDEPS
