@info "Installing dependencies"

using Pkg

packages = ["CSV",
            "DataFrames",
            "DSP",
            "FFTW",
            "JLD2",
            "MATLAB",
            "MLJ",
            "MLJDecisionTreeInterface",
            "Plots",
            "Random",
            "StatsBase"]
Pkg.add(packages)
