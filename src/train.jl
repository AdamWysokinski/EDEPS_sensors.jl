@info "Importing packages"

# config
optimize_regressor = false
optimize_classifier = false

using Pkg

# packages = ["CSV", "DataFrames", "DSP", "FFTW", "JLD2", "MLJ", "MLJDecisionTreeInterface", "Plots", "StatsBase"]
# Pkg.add(packages)

using CSV
using DataFrames
using DSP
using FFTW
using JLD2
using MLJ
using MLJDecisionTreeInterface
using Random
using Plots
using StatsBase

FFTW.set_num_threads(Sys.CPU_THREADS - 1)

m = Pkg.Operations.Context().env.manifest
println("       CSV $(m[findfirst(v -> v.name == "CSV", m)].version)")
println("DataFrames $(m[findfirst(v -> v.name == "DataFrames", m)].version)")
println("       DSP $(m[findfirst(v -> v.name == "DSP", m)].version)")
println("      FFTW $(m[findfirst(v -> v.name == "FFTW", m)].version)")
println("      JLD2 $(m[findfirst(v -> v.name == "JLD2", m)].version)")
println("       MLJ $(m[findfirst(v -> v.name == "MLJ", m)].version)")
println("   MLJ DTI $(m[findfirst(v -> v.name == "MLJDecisionTreeInterface", m)].version) (MLJDecisionTreeInterface)")
println("     Plots $(m[findfirst(v -> v.name == "Plots", m)].version)")
println(" StatsBase $(m[findfirst(v -> v.name == "StatsBase", m)].version)")
println()

function generate_psd(epoch_n, acc_x, acc_y, acc_z, ang_x, ang_y, ang_z)

    fs = 50

    p = welch_pgram(acc_x[1, :], 4*fs, fs=fs)
    f = Vector(freq(p))

    acc_x_psd = zeros(epoch_n, length(power(p)))
    acc_y_psd = zeros(epoch_n, length(power(p)))
    acc_z_psd = zeros(epoch_n, length(power(p)))
    ang_x_psd = zeros(epoch_n, length(power(p)))
    ang_y_psd = zeros(epoch_n, length(power(p)))
    ang_z_psd = zeros(epoch_n, length(power(p)))

    for idx in 1:epoch_n
        acc_x_psd[idx, :] = power(welch_pgram(acc_x[idx, :], 4*fs, fs=fs, window=DSP.hanning))
        acc_y_psd[idx, :] = power(welch_pgram(acc_y[idx, :], 4*fs, fs=fs, window=DSP.hanning))
        acc_z_psd[idx, :] = power(welch_pgram(acc_z[idx, :], 4*fs, fs=fs, window=DSP.hanning))
        ang_x_psd[idx, :] = power(welch_pgram(ang_x[idx, :], 4*fs, fs=fs, window=DSP.hanning))
        ang_y_psd[idx, :] = power(welch_pgram(ang_y[idx, :], 4*fs, fs=fs, window=DSP.hanning))
        ang_z_psd[idx, :] = power(welch_pgram(ang_z[idx, :], 4*fs, fs=fs, window=DSP.hanning))
    end

    acc_x_psd = acc_x_psd[:, 5:end]
    acc_y_psd = acc_y_psd[:, 5:end]
    acc_z_psd = acc_z_psd[:, 5:end]
    ang_x_psd = ang_x_psd[:, 5:end]
    ang_y_psd = ang_y_psd[:, 5:end]
    ang_z_psd = ang_z_psd[:, 5:end]

    return acc_x_psd, acc_y_psd, acc_z_psd, ang_x_psd, ang_y_psd, ang_z_psd, f

end

@info "Creating classifier model: RandomForestClassifier"

# load training data
@assert isfile("data/training_epochs.csv") "File data/training_epochs.csv cannot be opened!"
println("Loading: training_epochs.csv")
training_data = CSV.read("data/training_epochs.csv", header=false, DataFrame)

y = string.(training_data[:, 1])
replace!(y, "0.0" => "no tremor")
replace!(y, "1.0" => "tremor")
x = Matrix(training_data[:, 2:end])

println("Number of epochs: $(length(y)) [no tremor: $(count(y .== "no tremor")) / tremor: $(count(y .== "tremor"))]")

# PSD
println("Processing: PSD")
epoch_n = size(x, 1)

acc_x = x[1:epoch_n, 1:500]
acc_y = x[1:epoch_n, 501:1000]
acc_z = x[1:epoch_n, 1001:1500]
ang_x = x[1:epoch_n, 1501:2000]
ang_y = x[1:epoch_n, 2001:2500]
ang_z = x[1:epoch_n, 2501:3000]

acc_x_psd, acc_y_psd, acc_z_psd, ang_x_psd, ang_y_psd, ang_z_psd = generate_psd(epoch_n, acc_x, acc_y, acc_z, ang_x, ang_y, ang_z)

x = hcat(acc_x_psd, acc_y_psd, acc_z_psd, ang_x_psd, ang_y_psd, ang_z_psd)

# standardize
println("Processing: standardize")
classifier_scaler = StatsBase.fit(ZScoreTransform, x, dims=1)
x = StatsBase.transform(classifier_scaler, x)
println()

x = DataFrame(x, :auto)
y = DataFrame(group=y)
y = coerce(y.group, OrderedFactor{2})

Random.seed!(123)
rfc = @MLJ.load RandomForestClassifier pkg=DecisionTree verbosity=0
train_idx, test_idx = partition(eachindex(y), 0.7, rng=123) # 70:30 split

if optimize_classifier
    # info(RandomForestClassifier)
    n_trees_range = range(Int, :n_trees, lower=1, upper=1000)
    max_depth_range = range(Int, :max_depth, lower=1, upper=1000)
    min_samples_leaf_range = range(Int, :min_samples_leaf, lower=1, upper=100)
    min_samples_split_range = range(Int, :min_samples_split, lower=1, upper=100)
    min_purity_increase_range = range(Float64, :min_purity_increase, lower=0.1, upper=1.0)
    sampling_fraction_range = range(Float64, :sampling_fraction, lower=0.1, upper=1.0)
    p = [n_trees_range, max_depth_range, min_samples_leaf_range, min_samples_split_range, min_purity_increase_range, sampling_fraction_range]
    m = [log_loss, auc, accuracy, f1score, misclassification_rate, cross_entropy]
    # split
    Random.seed!(123)
    self_tuning_rfc1 = TunedModel(model=rfc(feature_importance=:split),
                                  resampling=CV(nfolds=2),
                                  tuning=Grid(resolution=20),
                                  range=p,
                                  measure=m)
    m_self_tuning_rfc1 = machine(self_tuning_rfc1, x, y)
    MLJ.fit!(m_self_tuning_rfc1)
    fitted_params(m_self_tuning_rfc1).best_model
    report(m_self_tuning_rfc1).best_history_entry
    # impurity
    Random.seed!(123)
    self_tuning_rfc2 = TunedModel(model=rfc(feature_importance=:impurity),
                                  resampling=CV(nfolds=5),
                                  tuning=Grid(resolution=5),
                                  range=p,
                                  measure=m)
    m_self_tuning_rfc2 = machine(self_tuning_rfc2, x, y)
    MLJ.fit!(m_self_tuning_rfc2)
    fitted_params(m_self_tuning_rfc2).best_model
    report(m_self_tuning_rfc2).best_history_entry

    model1 = fitted_params(m_self_tuning_rfc1).best_model
    model2 = fitted_params(m_self_tuning_rfc2).best_model

    evaluate(model1,
             x, y,
             resampling=CV(nfolds=10),
             measures=[log_loss, accuracy, f1score, misclassification_rate, cross_entropy])
    evaluate(model2,
             x, y,
             resampling=CV(nfolds=10),
             measures=[log_loss, accuracy, f1score, misclassification_rate, cross_entropy])

    report(m_self_tuning_rfc1).best_history_entry
    report(m_self_tuning_rfc2).best_history_entry
end

rfc_model = rfc(max_depth = -1, 
                min_samples_leaf = 1, 
                min_samples_split = 2, 
                min_purity_increase = 0.0, 
                n_subfeatures = -1, 
                n_trees = 250, 
                sampling_fraction = 0.99, 
                feature_importance = :impurity)

println("Classifier accuracy: training")
mach_class = machine(rfc_model, x[train_idx, :], y[train_idx])
MLJ.fit!(mach_class, force=true, verbosity=0)
yhat = MLJ.predict(mach_class, x[train_idx, :])
println("\tlog_loss: ", round(log_loss(yhat, y[train_idx]) |> mean, digits=4))
println("\tAUC: ", round(auc(yhat, y[train_idx]), digits=4))
println("\tmisclassification rate: ", round(misclassification_rate(mode.(yhat), y[train_idx]), digits=2))
println("\taccuracy: ", round(1 - misclassification_rate(mode.(yhat), y[train_idx]), digits=2))
println("confusion matrix:")
cm = confusion_matrix(mode.(yhat), y[train_idx])
println("\tsensitivity (TPR): ", round(cm.mat[1, 1] / sum(cm.mat[:, 1]), digits=2))
println("\tspecificity (TNR): ", round(cm.mat[2, 2] / sum(cm.mat[:, 2]), digits=2))
println("""
             ┌───────────────────┐
             │       Group       │
┌────────────┼─────────┬─────────┤
│ Prediction │no tremor│ tremor  │
├────────────┼─────────┼─────────┤
│ no tremor  │ $(rpad(cm.mat[1, 1], 4, " "))    │ $(rpad(cm.mat[1, 2], 4, " "))    │
├────────────┼─────────┼─────────┤
│   tremor   │ $(rpad(cm.mat[2, 1], 4, " "))    │ $(rpad(cm.mat[2, 2], 4, " "))    │
└────────────┴─────────┴─────────┘""")
println()

println("Classifier accuracy: validating")
yhat = MLJ.predict(mach_class, x[test_idx, :])
println("\tlog_loss: ", round(log_loss(yhat, y[test_idx]) |> mean, digits=4))
println("\tAUC: ", round(auc(yhat, y[test_idx]), digits=4))
println("\tmisclassification rate: ", round(misclassification_rate(mode.(yhat), y[test_idx]), digits=2))
println("\taccuracy: ", round(1 - misclassification_rate(mode.(yhat), y[test_idx]), digits=2))
println("confusion matrix:")
cm = confusion_matrix(mode.(yhat), y[test_idx])
println("\tsensitivity (TPR): ", round(cm.mat[1, 1] / sum(cm.mat[:, 1]), digits=2))
println("\tspecificity (TNR): ", round(cm.mat[2, 2] / sum(cm.mat[:, 2]), digits=2))
println("""
             ┌───────────────────┐
             │       Group       │
┌────────────┼─────────┬─────────┤
│ Prediction │no tremor│ tremor  │
├────────────┼─────────┼─────────┤
│ no tremor  │ $(rpad(cm.mat[1, 1], 4, " "))    │ $(rpad(cm.mat[1, 2], 4, " "))    │
├────────────┼─────────┼─────────┤
│   tremor   │ $(rpad(cm.mat[2, 1], 4, " "))    │ $(rpad(cm.mat[2, 2], 4, " "))    │
└────────────┴─────────┴─────────┘""")
println()

println("Classifier accuracy: final model")
mach_class = machine(rfc_model, x, y)
MLJ.fit!(mach_class, force=true, verbosity=0)
yhat = MLJ.predict(mach_class, x)
println("\tlog_loss: ", round(log_loss(yhat, y) |> mean, digits=4))
println("\tAUC: ", round(auc(yhat, y), digits=4))
println("\tmisclassification rate: ", round(misclassification_rate(mode.(yhat), y), digits=2))
println("\taccuracy: ", round(1 - misclassification_rate(mode.(yhat), y), digits=2))
println("confusion matrix:")
cm = confusion_matrix(mode.(yhat), y)
println("\tsensitivity (TPR): ", round(cm.mat[1, 1] / sum(cm.mat[:, 1]), digits=2))
println("\tspecificity (TNR): ", round(cm.mat[2, 2] / sum(cm.mat[:, 2]), digits=2))
println("""
             ┌───────────────────┐
             │       Group       │
┌────────────┼─────────┬─────────┤
│ Prediction │no tremor│ tremor  │
├────────────┼─────────┼─────────┤
│ no tremor  │ $(rpad(cm.mat[1, 1], 4, " "))    │ $(rpad(cm.mat[1, 2], 4, " "))    │
├────────────┼─────────┼─────────┤
│   tremor   │ $(rpad(cm.mat[2, 1], 4, " "))    │ $(rpad(cm.mat[2, 2], 4, " "))    │
└────────────┴─────────┴─────────┘""")
println()

println("Saving: classifier_model.jlso")
MLJ.save("models/classifier_model.jlso", mach_class)
println("Saving: classifier_scaler.jld")
JLD2.save_object("models/classifier_scaler.jld", classifier_scaler)
println()

@info "Creating regressor model: RandomForestRegressor"

# load training data
@assert isfile("data/training_epochs_regressor.csv") "File data/training_epochs_regressor.csv cannot be opened!"
println("Loading: training_epochs_regressor.csv")
training_data = CSV.read("data/training_epochs_regressor.csv", header=false, DataFrame)

y = training_data[:, 2:5]
x = Matrix(training_data[:, 6:end])

println("Number of epochs: $(size(y, 1))")

# PSD
println("Processing: PSD")
epoch_n = size(x, 1)

acc_x = x[1:epoch_n, 1:500]
acc_y = x[1:epoch_n, 501:1000]
acc_z = x[1:epoch_n, 1001:1500]
ang_x = x[1:epoch_n, 1501:2000]
ang_y = x[1:epoch_n, 2001:2500]
ang_z = x[1:epoch_n, 2501:3000]

acc_x_psd, acc_y_psd, acc_z_psd, ang_x_psd, ang_y_psd, ang_z_psd = generate_psd(epoch_n, acc_x, acc_y, acc_z, ang_x, ang_y, ang_z)
x = hcat(acc_x_psd, acc_y_psd, acc_z_psd, ang_x_psd, ang_y_psd, ang_z_psd)

# standardize
println("Processing: standardize")
regressor_scaler = StatsBase.fit(ZScoreTransform, x, dims=1) 
x = StatsBase.transform(regressor_scaler, x)
println()

x = DataFrame(x, :auto)

# RF: train the model SAS
println("Creating model: SAS")
Y = y[:, 1]
rfr = @MLJ.load RandomForestRegressor pkg=DecisionTree verbosity=0
train_idx, test_idx = partition(eachindex(Y), 0.7, rng=123) # 70:30 split

if optimize_regressor
    # info(RandomForestRegressor)
    n_trees_range = range(Int, :n_trees, lower=1, upper=1000)
    max_depth_range = range(Int, :max_depth, lower=1, upper=1000)
    min_samples_leaf_range = range(Int, :min_samples_leaf, lower=1, upper=100)
    min_samples_split_range = range(Int, :min_samples_split, lower=1, upper=100)
    min_purity_increase_range = range(Float64, :min_purity_increase, lower=0.1, upper=1.0)
    sampling_fraction_range = range(Float64, :sampling_fraction, lower=0.1, upper=1.0)
    p = [n_trees_range, max_depth_range, min_samples_leaf_range, min_samples_split_range, min_purity_increase_range, sampling_fraction_range]
    m = [root_mean_squared_error, rsquared]
    # split
    Random.seed!(123)
    self_tuning_rfc1 = TunedModel(model=rfr(feature_importance=:split),
                                  resampling=CV(nfolds=2),
                                  tuning=Grid(resolution=20),
                                  range=p,
                                  measure=m)
    m_self_tuning_rfr1 = machine(self_tuning_rfr1, x, y)
    MLJ.fit!(m_self_tuning_rfr1)
    fitted_params(m_self_tuning_rfr1).best_model
    report(m_self_tuning_rfr1).best_history_entry
    # impurity
    Random.seed!(123)
    self_tuning_rfr2 = TunedModel(model=rfr(feature_importance=:impurity),
                                  resampling=CV(nfolds=5),
                                  tuning=Grid(resolution=5),
                                  range=p,
                                  measure=m)
    m_self_tuning_rfr2 = machine(self_tuning_rfr2, x, y)
    MLJ.fit!(m_self_tuning_rfr2)
    fitted_params(m_self_tuning_rfr2).best_model
    report(m_self_tuning_rfr2).best_history_entry

    model1 = fitted_params(m_self_tuning_rfr1).best_model
    model2 = fitted_params(m_self_tuning_rfr2).best_model

    evaluate(model1,
             x, y,
             resampling=CV(nfolds=10),
             measures=[root_mean_squared_error, rsquared])
    evaluate(model2,
             x, y,
             resampling=CV(nfolds=10),
             measures=[root_mean_squared_error, rsquared])

    report(m_self_tuning_rfr1).best_history_entry
    report(m_self_tuning_rfr2).best_history_entry
end

Random.seed!(123)
rfr_model = rfr(max_depth = -1, 
                min_samples_leaf = 1, 
                min_samples_split = 2, 
                min_purity_increase = 0.0, 
                n_subfeatures = -1, 
                n_trees = 260, 
                sampling_fraction = 0.99, 
                feature_importance = :impurity)

println("Regressor accuracy: training")
mach_sas = machine(rfr_model, x[train_idx, :], Y[train_idx], scitype_check_level=0)
MLJ.fit!(mach_sas, force=true, verbosity=0)
yhat = round.(MLJ.predict(mach_sas, x[train_idx, :]), digits=0)
m = RSquared()
println("\tR²: ", round(m(yhat, Y[train_idx]), digits=4))
m = RootMeanSquaredError()
println("\tRMSE: ", round(m(yhat, Y[train_idx]), digits=4))
println()

println("Regressor accuracy: testing")
yhat = round.(MLJ.predict(mach_sas, x[test_idx, :]), digits=0)
m = RSquared()
println("\tR²: ", round(m(yhat, Y[test_idx]), digits=4))
m = RootMeanSquaredError()
println("\tRMSE: ", round(m(yhat, Y[test_idx]), digits=4))
println()

println("Regressor accuracy: final model")
mach_sas = machine(rfr_model, x, Y, scitype_check_level=0)
MLJ.fit!(mach_sas, force=true, verbosity=0)
yhat = round.(MLJ.predict(mach_sas, x), digits=0)
p1 = Plots.scatter(Y .- yhat, ylims=(-4, +4), xlabel="epochs", ylabel="SAS error", ms=1, mc=:black, label=false)
m = RSquared()
println("\tR²: ", round(m(yhat, Y), digits=4))
m = RootMeanSquaredError()
println("\tRMSE: ", round(m(yhat, Y), digits=4))
println()

# RF: train the model AIMS8
println("Creating model: AIMS8")
Y = y[:, 2]
rfr = @MLJ.load RandomForestRegressor pkg=DecisionTree verbosity=0

if optimize_regressor
    # info(RandomForestRegressor)
    n_trees_range = range(Int, :n_trees, lower=1, upper=1000)
    max_depth_range = range(Int, :max_depth, lower=1, upper=1000)
    min_samples_leaf_range = range(Int, :min_samples_leaf, lower=1, upper=100)
    min_samples_split_range = range(Int, :min_samples_split, lower=1, upper=100)
    min_purity_increase_range = range(Float64, :min_purity_increase, lower=0.1, upper=1.0)
    sampling_fraction_range = range(Float64, :sampling_fraction, lower=0.1, upper=1.0)
    p = [n_trees_range, max_depth_range, min_samples_leaf_range, min_samples_split_range, min_purity_increase_range, sampling_fraction_range]
    m = [root_mean_squared_error, rsquared]
    # split
    Random.seed!(123)
    self_tuning_rfc1 = TunedModel(model=rfr(feature_importance=:split),
                                  resampling=CV(nfolds=2),
                                  tuning=Grid(resolution=20),
                                  range=p,
                                  measure=m)
    m_self_tuning_rfr1 = machine(self_tuning_rfr1, x, y)
    MLJ.fit!(m_self_tuning_rfr1)
    fitted_params(m_self_tuning_rfr1).best_model
    report(m_self_tuning_rfr1).best_history_entry
    # impurity
    Random.seed!(123)
    self_tuning_rfr2 = TunedModel(model=rfr(feature_importance=:impurity),
                                  resampling=CV(nfolds=5),
                                  tuning=Grid(resolution=5),
                                  range=p,
                                  measure=m)
    m_self_tuning_rfr2 = machine(self_tuning_rfr2, x, y)
    MLJ.fit!(m_self_tuning_rfr2)
    fitted_params(m_self_tuning_rfr2).best_model
    report(m_self_tuning_rfr2).best_history_entry

    model1 = fitted_params(m_self_tuning_rfr1).best_model
    model2 = fitted_params(m_self_tuning_rfr2).best_model

    evaluate(model1,
             x, y,
             resampling=CV(nfolds=10),
             measures=[root_mean_squared_error, rsquared])
    evaluate(model2,
             x, y,
             resampling=CV(nfolds=10),
             measures=[root_mean_squared_error, rsquared])

    report(m_self_tuning_rfr1).best_history_entry
    report(m_self_tuning_rfr2).best_history_entry
end

rfr_model = rfr(max_depth = -1, 
                min_samples_leaf = 1, 
                min_samples_split = 2, 
                min_purity_increase = 0.0, 
                n_subfeatures = -1, 
                n_trees = 260, 
                sampling_fraction = 0.99, 
                feature_importance = :impurity)

println("Regressor accuracy: training")
mach_aims8 = machine(rfr_model, x[train_idx, :], Y[train_idx], scitype_check_level=0)
MLJ.fit!(mach_aims8, force=true, verbosity=0)
yhat = round.(MLJ.predict(mach_aims8, x[train_idx, :]), digits=0)
m = RSquared()
println("\tR²: ", round(m(yhat, Y[train_idx]), digits=4))
m = RootMeanSquaredError()
println("\tRMSE: ", round(m(yhat, Y[train_idx]), digits=4))
println()

println("Regressor accuracy: testing")
yhat = round.(MLJ.predict(mach_aims8, x[test_idx, :]), digits=0)
m = RSquared()
println("\tR²: ", round(m(yhat, Y[test_idx]), digits=4))
m = RootMeanSquaredError()
println("\tRMSE: ", round(m(yhat, Y[test_idx]), digits=4))
println()

println("Regressor accuracy: final model")
mach_aims8 = machine(rfr_model, x, Y, scitype_check_level=0)
MLJ.fit!(mach_aims8, force=true, verbosity=0)
yhat = round.(MLJ.predict(mach_aims8, x), digits=0)
p2 = Plots.scatter(Y .- yhat, ylims=(-4, +4), xlabel="epochs", ylabel="AIMS8 error", ms=1, mc=:black, label=false)
m = RSquared()
println("\tR²: ", round(m(yhat, Y), digits=4))
m = RootMeanSquaredError()
println("\tRMSE: ", round(m(yhat, Y), digits=4))
println()

# RF: train the model AIMS9
println("Creating model: AIMS9")
Y = y[:, 3]
rfr = @MLJ.load RandomForestRegressor pkg=DecisionTree verbosity=0

if optimize_regressor
    # info(RandomForestRegressor)
    n_trees_range = range(Int, :n_trees, lower=1, upper=1000)
    max_depth_range = range(Int, :max_depth, lower=1, upper=1000)
    min_samples_leaf_range = range(Int, :min_samples_leaf, lower=1, upper=100)
    min_samples_split_range = range(Int, :min_samples_split, lower=1, upper=100)
    min_purity_increase_range = range(Float64, :min_purity_increase, lower=0.1, upper=1.0)
    sampling_fraction_range = range(Float64, :sampling_fraction, lower=0.1, upper=1.0)
    p = [n_trees_range, max_depth_range, min_samples_leaf_range, min_samples_split_range, min_purity_increase_range, sampling_fraction_range]
    m = [root_mean_squared_error, rsquared]
    # split
    Random.seed!(123)
    self_tuning_rfc1 = TunedModel(model=rfr(feature_importance=:split),
                                  resampling=CV(nfolds=2),
                                  tuning=Grid(resolution=20),
                                  range=p,
                                  measure=m)
    m_self_tuning_rfr1 = machine(self_tuning_rfr1, x, y)
    MLJ.fit!(m_self_tuning_rfr1)
    fitted_params(m_self_tuning_rfr1).best_model
    report(m_self_tuning_rfr1).best_history_entry
    # impurity
    Random.seed!(123)
    self_tuning_rfr2 = TunedModel(model=rfr(feature_importance=:impurity),
                                  resampling=CV(nfolds=5),
                                  tuning=Grid(resolution=5),
                                  range=p,
                                  measure=m)
    m_self_tuning_rfr2 = machine(self_tuning_rfr2, x, y)
    MLJ.fit!(m_self_tuning_rfr2)
    fitted_params(m_self_tuning_rfr2).best_model
    report(m_self_tuning_rfr2).best_history_entry

    model1 = fitted_params(m_self_tuning_rfr1).best_model
    model2 = fitted_params(m_self_tuning_rfr2).best_model

    evaluate(model1,
             x, y,
             resampling=CV(nfolds=10),
             measures=[root_mean_squared_error, rsquared])
    evaluate(model2,
             x, y,
             resampling=CV(nfolds=10),
             measures=[root_mean_squared_error, rsquared])

    report(m_self_tuning_rfr1).best_history_entry
    report(m_self_tuning_rfr2).best_history_entry
end

rfr_model = rfr(max_depth = -1, 
                min_samples_leaf = 1, 
                min_samples_split = 2, 
                min_purity_increase = 0.0, 
                n_subfeatures = -1, 
                n_trees = 260, 
                sampling_fraction = 0.99, 
                feature_importance = :impurity)

println("Regressor accuracy: training")
mach_aims9 = machine(rfr_model, x[train_idx, :], Y[train_idx], scitype_check_level=0)
MLJ.fit!(mach_aims9, force=true, verbosity=0)
yhat = round.(MLJ.predict(mach_aims9, x[train_idx, :]), digits=0)
m = RSquared()
println("\tR²: ", round(m(yhat, Y[train_idx]), digits=4))
m = RootMeanSquaredError()
println("\tRMSE: ", round(m(yhat, Y[train_idx]), digits=4))
println()

println("Regressor accuracy: testing")
yhat = round.(MLJ.predict(mach_aims9, x[test_idx, :]), digits=0)
m = RSquared()
println("\tR²: ", round(m(yhat, Y[test_idx]), digits=4))
m = RootMeanSquaredError()
println("\tRMSE: ", round(m(yhat, Y[test_idx]), digits=4))
println()

println("Regressor accuracy: final model")
mach_aims9 = machine(rfr_model, x, Y, scitype_check_level=0)
MLJ.fit!(mach_aims9, force=true, verbosity=0)
yhat = round.(MLJ.predict(mach_aims9, x), digits=0)
p3 = Plots.scatter(Y .- yhat, ylims=(-4, +4), xlabel="epochs", ylabel="AIMS9 error", ms=1, mc=:black, label=false)
m = RSquared()
println("\tR²: ", round(m(yhat, Y), digits=4))
m = RootMeanSquaredError()
println("\tRMSE: ", round(m(yhat, Y), digits=4))
println()

# RF: train the model AIMS10
println("Creating model: AIMS10")
Y = y[:, 4]
rfr = @MLJ.load RandomForestRegressor pkg=DecisionTree verbosity=0

if optimize_regressor
    # info(RandomForestRegressor)
    n_trees_range = range(Int, :n_trees, lower=1, upper=1000)
    max_depth_range = range(Int, :max_depth, lower=1, upper=1000)
    min_samples_leaf_range = range(Int, :min_samples_leaf, lower=1, upper=100)
    min_samples_split_range = range(Int, :min_samples_split, lower=1, upper=100)
    min_purity_increase_range = range(Float64, :min_purity_increase, lower=0.1, upper=1.0)
    sampling_fraction_range = range(Float64, :sampling_fraction, lower=0.1, upper=1.0)
    p = [n_trees_range, max_depth_range, min_samples_leaf_range, min_samples_split_range, min_purity_increase_range, sampling_fraction_range]
    m = [root_mean_squared_error, rsquared]
    # split
    Random.seed!(123)
    self_tuning_rfc1 = TunedModel(model=rfr(feature_importance=:split),
                                  resampling=CV(nfolds=2),
                                  tuning=Grid(resolution=20),
                                  range=p,
                                  measure=m)
    m_self_tuning_rfr1 = machine(self_tuning_rfr1, x, y)
    MLJ.fit!(m_self_tuning_rfr1)
    fitted_params(m_self_tuning_rfr1).best_model
    report(m_self_tuning_rfr1).best_history_entry
    # impurity
    Random.seed!(123)
    self_tuning_rfr2 = TunedModel(model=rfr(feature_importance=:impurity),
                                  resampling=CV(nfolds=5),
                                  tuning=Grid(resolution=5),
                                  range=p,
                                  measure=m)
    m_self_tuning_rfr2 = machine(self_tuning_rfr2, x, y)
    MLJ.fit!(m_self_tuning_rfr2)
    fitted_params(m_self_tuning_rfr2).best_model
    report(m_self_tuning_rfr2).best_history_entry

    model1 = fitted_params(m_self_tuning_rfr1).best_model
    model2 = fitted_params(m_self_tuning_rfr2).best_model

    evaluate(model1,
             x, y,
             resampling=CV(nfolds=10),
             measures=[root_mean_squared_error, rsquared])
    evaluate(model2,
             x, y,
             resampling=CV(nfolds=10),
             measures=[root_mean_squared_error, rsquared])

    report(m_self_tuning_rfr1).best_history_entry
    report(m_self_tuning_rfr2).best_history_entry
end

rfr_model = rfr(max_depth = -1, 
                min_samples_leaf = 1, 
                min_samples_split = 2, 
                min_purity_increase = 0.0, 
                n_subfeatures = -1, 
                n_trees = 260, 
                sampling_fraction = 0.99, 
                feature_importance = :impurity)

println("Regressor accuracy: training")
mach_aims10 = machine(rfr_model, x[train_idx, :], Y[train_idx], scitype_check_level=0)
MLJ.fit!(mach_aims10, force=true, verbosity=0)
yhat = round.(MLJ.predict(mach_aims10, x[train_idx, :]), digits=0)
m = RSquared()
println("\tR²: ", round(m(yhat, Y[train_idx]), digits=4))
m = RootMeanSquaredError()
println("\tRMSE: ", round(m(yhat, Y[train_idx]), digits=4))
println()

println("Regressor accuracy: testing")
yhat = round.(MLJ.predict(mach_aims10, x[test_idx, :]), digits=0)
m = RSquared()
println("\tR²: ", round(m(yhat, Y[test_idx]), digits=4))
m = RootMeanSquaredError()
println("\tRMSE: ", round(m(yhat, Y[test_idx]), digits=4))
println()

println("Regressor accuracy: final model")
mach_aims10 = machine(rfr_model, x, Y, scitype_check_level=0)
MLJ.fit!(mach_aims10, force=true, verbosity=0)
yhat = round.(MLJ.predict(mach_aims10, x), digits=0)
p4 = Plots.scatter(Y .- yhat, ylims=(-4, +4), xlabel="epochs", ylabel="AIMS10 error", ms=1, mc=:black, label=false)
m = RSquared()
println("\tR²: ", round(m(yhat, Y), digits=4))
m = RootMeanSquaredError()
println("\tRMSE: ", round(m(yhat, Y), digits=4))
println()

# RF: train the model AIMS
println("Creating model: AIMS")
Y = y[:, 2:4]
Y = sum.(eachrow(Y[:, names(Y, Real)]))
rfr = @MLJ.load RandomForestRegressor pkg=DecisionTree verbosity=0

if optimize_regressor
    # info(RandomForestRegressor)
    n_trees_range = range(Int, :n_trees, lower=1, upper=1000)
    max_depth_range = range(Int, :max_depth, lower=1, upper=1000)
    min_samples_leaf_range = range(Int, :min_samples_leaf, lower=1, upper=100)
    min_samples_split_range = range(Int, :min_samples_split, lower=1, upper=100)
    min_purity_increase_range = range(Float64, :min_purity_increase, lower=0.1, upper=1.0)
    sampling_fraction_range = range(Float64, :sampling_fraction, lower=0.1, upper=1.0)
    p = [n_trees_range, max_depth_range, min_samples_leaf_range, min_samples_split_range, min_purity_increase_range, sampling_fraction_range]
    m = [root_mean_squared_error, rsquared]
    # split
    Random.seed!(123)
    self_tuning_rfc1 = TunedModel(model=rfr(feature_importance=:split),
                                  resampling=CV(nfolds=2),
                                  tuning=Grid(resolution=20),
                                  range=p,
                                  measure=m)
    m_self_tuning_rfr1 = machine(self_tuning_rfr1, x, y)
    MLJ.fit!(m_self_tuning_rfr1)
    fitted_params(m_self_tuning_rfr1).best_model
    report(m_self_tuning_rfr1).best_history_entry
    # impurity
    Random.seed!(123)
    self_tuning_rfr2 = TunedModel(model=rfr(feature_importance=:impurity),
                                  resampling=CV(nfolds=5),
                                  tuning=Grid(resolution=5),
                                  range=p,
                                  measure=m)
    m_self_tuning_rfr2 = machine(self_tuning_rfr2, x, y)
    MLJ.fit!(m_self_tuning_rfr2)
    fitted_params(m_self_tuning_rfr2).best_model
    report(m_self_tuning_rfr2).best_history_entry

    model1 = fitted_params(m_self_tuning_rfr1).best_model
    model2 = fitted_params(m_self_tuning_rfr2).best_model

    evaluate(model1,
             x, y,
             resampling=CV(nfolds=10),
             measures=[root_mean_squared_error, rsquared])
    evaluate(model2,
             x, y,
             resampling=CV(nfolds=10),
             measures=[root_mean_squared_error, rsquared])

    report(m_self_tuning_rfr1).best_history_entry
    report(m_self_tuning_rfr2).best_history_entry
end

rfr_model = rfr(max_depth = -1, 
                min_samples_leaf = 1, 
                min_samples_split = 2, 
                min_purity_increase = 0.0, 
                n_subfeatures = -1, 
                n_trees = 260, 
                sampling_fraction = 0.99, 
                feature_importance = :impurity)

println("Regressor accuracy: training")
mach_aims = machine(rfr_model, x[train_idx, :], Y[train_idx], scitype_check_level=0)
MLJ.fit!(mach_aims, force=true, verbosity=0)
yhat = round.(MLJ.predict(mach_aims, x[train_idx, :]), digits=0)
m = RSquared()
println("\tR²: ", round(m(yhat, Y[train_idx]), digits=4))
m = RootMeanSquaredError()
println("\tRMSE: ", round(m(yhat, Y[train_idx]), digits=4))
println()

println("Regressor accuracy: testing")
yhat = round.(MLJ.predict(mach_aims, x[test_idx, :]), digits=0)
m = RSquared()
println("\tR²: ", round(m(yhat, Y[test_idx]), digits=4))
m = RootMeanSquaredError()
println("\tRMSE: ", round(m(yhat, Y[test_idx]), digits=4))
println()

println("Regressor accuracy: final model")
mach_aims = machine(rfr_model, x, Y, scitype_check_level=0)
MLJ.fit!(mach_aims, force=true, verbosity=0)
yhat = round.(MLJ.predict(mach_aims, x), digits=0)
p5 = Plots.scatter(Y .- yhat, ylims=(-4, +4), xlabel="epochs", ylabel="AIMS error", ms=1, mc=:black, label=false)
m = RSquared()
println("\tR²: ", round(m(yhat, Y), digits=4))
m = RootMeanSquaredError()
println("\tRMSE: ", round(m(yhat, Y), digits=4))
println()

p_aims = Plots.plot(p2, p3, p4, p5, layout=(2, 2))
p = Plots.plot(p1, p_aims, layout=(2, 1), xlabelfontsize=6, ylabelfontsize=6, xtickfontsize=4, ytickfontsize=4, legendfontsize=4)
savefig(p, "reports/train_regressor.png")

println("Saving: regressor_model_sas.jlso")
MLJ.save("models/regressor_model_sas.jlso", mach_sas)
println("Saving: regressor_model_aims8.jlso")
MLJ.save("models/regressor_model_aims8.jlso", mach_aims8)
println("Saving: regressor_model_aims9.jlso")
MLJ.save("models/regressor_model_aims9.jlso", mach_aims9)
println("Saving: regressor_model_aims10.jlso")
MLJ.save("models/regressor_model_aims10.jlso", mach_aims10)
println("Saving: regressor_model_aims.jlso")
MLJ.save("models/regressor_model_aims.jlso", mach_aims)
println("Saving: regressor_scaler.jld")
JLD2.save_object("models/regressor_scaler.jld", regressor_scaler)
