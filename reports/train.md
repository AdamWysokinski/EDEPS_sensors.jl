       CSV 0.10.15
DataFrames 1.7.0
       DSP 0.7.10
      FFTW 1.8.0
      JLD2 0.4.53
       MLJ 0.20.7
   MLJ DTI 0.4.2 (MLJDecisionTreeInterface)
     Plots 1.40.7
 StatsBase 0.34.3

Loading: training_epochs.csv
Number of epochs: 2338 [no tremor: 1323 / tremor: 1015]
Processing: PSD
Processing: standardize

Classifier accuracy: training
	log_loss: 0.0406
	AUC: 1.0
	misclassification rate: 0.0
	accuracy: 1.0
confusion matrix:
	sensitivity (TPR): 1.0
	specificity (TNR): 1.0
             ┌───────────────────┐
             │       Group       │
┌────────────┼─────────┬─────────┤
│ Prediction │no tremor│ tremor  │
├────────────┼─────────┼─────────┤
│ no tremor  │ 922     │ 0       │
├────────────┼─────────┼─────────┤
│   tremor   │ 0       │ 715     │
└────────────┴─────────┴─────────┘

Classifier accuracy: validating
	log_loss: 0.1374
	AUC: 0.9917
	misclassification rate: 0.06
	accuracy: 0.94
confusion matrix:
	sensitivity (TPR): 0.95
	specificity (TNR): 0.93
             ┌───────────────────┐
             │       Group       │
┌────────────┼─────────┬─────────┤
│ Prediction │no tremor│ tremor  │
├────────────┼─────────┼─────────┤
│ no tremor  │ 380     │ 22      │
├────────────┼─────────┼─────────┤
│   tremor   │ 21      │ 278     │
└────────────┴─────────┴─────────┘

Classifier accuracy: final model
	log_loss: 0.0372
	AUC: 1.0
	misclassification rate: 0.0
	accuracy: 1.0
confusion matrix:
	sensitivity (TPR): 1.0
	specificity (TNR): 1.0
             ┌───────────────────┐
             │       Group       │
┌────────────┼─────────┬─────────┤
│ Prediction │no tremor│ tremor  │
├────────────┼─────────┼─────────┤
│ no tremor  │ 1323    │ 0       │
├────────────┼─────────┼─────────┤
│   tremor   │ 0       │ 1015    │
└────────────┴─────────┴─────────┘

Saving: classifier_model.jlso
Saving: classifier_scaler.jld

Loading: training_epochs_regressor.csv
Number of epochs: 1053
Processing: PSD
Processing: standardize

Creating model: SAS
Regressor accuracy: training
	R²: 0.96
	RMSE: 0.2116

Regressor accuracy: testing
	R²: 0.6556
	RMSE: 0.6162

Regressor accuracy: final model
	R²: 0.9471
	RMSE: 0.2427

Creating model: AIMS8
Regressor accuracy: training
	R²: 0.9358
	RMSE: 0.1688

Regressor accuracy: testing
	R²: 0.6531
	RMSE: 0.3938

Regressor accuracy: final model
	R²: 0.9018
	RMSE: 0.209

Creating model: AIMS9
Regressor accuracy: training
	R²: 0.9135
	RMSE: 0.2241

Regressor accuracy: testing
	R²: 0.3215
	RMSE: 0.6085

Regressor accuracy: final model
	R²: 0.8452
	RMSE: 0.2972

Creating model: AIMS10
Regressor accuracy: training
	R²: 0.9216
	RMSE: 0.2781

Regressor accuracy: testing
	R²: 0.4142
	RMSE: 0.7956

Regressor accuracy: final model
	R²: 0.87
	RMSE: 0.3633

Creating model: AIMS
Regressor accuracy: training
	R²: 0.904
	RMSE: 0.6548

Regressor accuracy: testing
	R²: 0.4727
	RMSE: 1.5711

Regressor accuracy: final model
	R²: 0.8414
	RMSE: 0.8479
