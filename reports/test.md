       CSV 0.10.15
DataFrames 1.7.0
       DSP 0.7.10
      FFTW 1.8.0
      JLD2 0.4.53
       MLJ 0.20.7
     Plots 1.40.7
 StatsBase 0.34.3

Loading: test_epochs_regressor.csv
Loading model: regressor_model_sas.jlso
Loading model: regressor_model_aims8.jlso
Loading model: regressor_model_aims9.jlso
Loading model: regressor_model_aims10.jlso
Loading model: regressor_model_aims.jlso
Loading: regressor_scaler.jld
Number of subjects: 29
Total number of epochs: 204
Processing: PSD
Processing: standardize

Calculating predictions: SAS, all available epochs
RandomForestRegressor accuracy report:
	R²: 0.7652
	RMSE: 0.5557

Calculating predictions: AIMS8, all available epochs
RandomForestRegressor accuracy report:
	R²: 0.7615
	RMSE: 0.5601

Calculating predictions: AIMS9, all available epochs
RandomForestRegressor accuracy report:
	R²: 0.4744
	RMSE: 0.8314

Calculating predictions: AIMS10, all available epochs
RandomForestRegressor accuracy report:
	R²: 0.5258
	RMSE: 1.029

Calculating predictions: AIMS, all available epochs
RandomForestRegressor accuracy report:
	R²: 0.5741
	RMSE: 2.4375

Calculating predictions: SAS, first epoch only
RandomForestRegressor accuracy report:
	R²: 0.4756
	RMSE: 0.8305

Calculating predictions: AIMS8, first epoch only
RandomForestRegressor accuracy report:
	R²: 0.5805
	RMSE: 0.7428

Calculating predictions: AIMS9, first epoch only
RandomForestRegressor accuracy report:
	R²: 0.4756
	RMSE: 0.8305

Calculating predictions: AIMS10, first epoch only
RandomForestRegressor accuracy report:
	R²: 0.5382
	RMSE: 1.0171

Calculating predictions: AIMS, first epoch only
RandomForestRegressor accuracy report:
	R²: 0.543
	RMSE: 2.5257

Loading: test_epochs.csv
Loading model: classifier_model.jlso
Loading: classifier_scaler.jld
Number of subjects: 88
Total number of epochs: 613
Processing: PSD
Processing: standardize

Calculating predictions: classifier, all available epochs
subject:   1 [epochs:   7] group: NO TREMOR, prediction: NO TREMOR, p = 0.98; adj. prediction: NO TREMOR, p = 1.00
subject:   2 [epochs:   7] group: NO TREMOR, prediction: NO TREMOR, p = 0.98; adj. prediction: NO TREMOR, p = 1.00
subject:   3 [epochs:   7] group: NO TREMOR, prediction: NO TREMOR, p = 0.96; adj. prediction: NO TREMOR, p = 1.00
subject:   4 [epochs:   7] group: NO TREMOR, prediction: NO TREMOR, p = 0.84; adj. prediction: NO TREMOR, p = 1.00
subject:   5 [epochs:   7] group: NO TREMOR, prediction: NO TREMOR, p = 0.77; adj. prediction: NO TREMOR, p = 1.00
subject:   6 [epochs:   7] group: NO TREMOR, prediction: NO TREMOR, p = 0.52; adj. prediction: NO TREMOR, p = 0.82
subject:   7 [epochs:   7] group: NO TREMOR, prediction: NO TREMOR, p = 0.98; adj. prediction: NO TREMOR, p = 0.88
subject:   8 [epochs:   8] group: NO TREMOR, prediction: NO TREMOR, p = 0.97; adj. prediction: NO TREMOR, p = 0.67
subject:   9 [epochs:   7] group: NO TREMOR, prediction: NO TREMOR, p = 0.95; adj. prediction: NO TREMOR, p = 0.85
subject:  10 [epochs:   7] group: NO TREMOR, prediction: NO TREMOR, p = 0.97; adj. prediction: NO TREMOR, p = 0.67
subject:  11 [epochs:   7] group: NO TREMOR, prediction: NO TREMOR, p = 0.98; adj. prediction: NO TREMOR, p = 0.68
subject:  12 [epochs:   7] group: NO TREMOR, prediction: NO TREMOR, p = 0.96; adj. prediction: NO TREMOR, p = 0.66
subject:  13 [epochs:   7] group: NO TREMOR, prediction: NO TREMOR, p = 0.78; adj. prediction: NO TREMOR, p = 1.00
subject:  14 [epochs:   7] group: NO TREMOR, prediction: NO TREMOR, p = 0.97; adj. prediction: NO TREMOR, p = 0.87
subject:  15 [epochs:   8] group: NO TREMOR, prediction: NO TREMOR, p = 0.80; adj. prediction: NO TREMOR, p = 0.90
subject:  16 [epochs:   7] group: NO TREMOR, prediction: NO TREMOR, p = 0.87; adj. prediction: NO TREMOR, p = 0.77
subject:  17 [epochs:   7] group: NO TREMOR, prediction: NO TREMOR, p = 0.80; adj. prediction: NO TREMOR, p = 1.00
subject:  18 [epochs:   7] group: NO TREMOR, prediction: NO TREMOR, p = 0.60; adj. prediction: NO TREMOR, p = 0.50
subject:  19 [epochs:   7] group: NO TREMOR, prediction: NO TREMOR, p = 0.94; adj. prediction: NO TREMOR, p = 1.00
subject:  20 [epochs:   7] group: NO TREMOR, prediction: NO TREMOR, p = 0.90; adj. prediction: NO TREMOR, p = 1.00
subject:  21 [epochs:   7] group: NO TREMOR, prediction: NO TREMOR, p = 0.89; adj. prediction: NO TREMOR, p = 1.00
subject:  22 [epochs:   8] group: NO TREMOR, prediction: NO TREMOR, p = 0.99; adj. prediction: NO TREMOR, p = 0.69
subject:  23 [epochs:   7] group: NO TREMOR, prediction: NO TREMOR, p = 0.96; adj. prediction: NO TREMOR, p = 0.86
subject:  24 [epochs:   7] group: NO TREMOR, prediction: NO TREMOR, p = 0.99; adj. prediction: NO TREMOR, p = 1.00
subject:  25 [epochs:   7] group: NO TREMOR, prediction: NO TREMOR, p = 0.88; adj. prediction: NO TREMOR, p = 0.98
subject:  26 [epochs:   7] group: NO TREMOR, prediction: NO TREMOR, p = 0.99; adj. prediction: NO TREMOR, p = 0.69
subject:  27 [epochs:   7] group: NO TREMOR, prediction: NO TREMOR, p = 0.99; adj. prediction: NO TREMOR, p = 0.69
subject:  28 [epochs:   7] group: NO TREMOR, prediction: NO TREMOR, p = 0.96; adj. prediction: NO TREMOR, p = 0.66
subject:  29 [epochs:   7] group: NO TREMOR, prediction: NO TREMOR, p = 1.00; adj. prediction: NO TREMOR, p = 0.70
subject:  30 [epochs:   7] group: NO TREMOR, prediction: NO TREMOR, p = 1.00; adj. prediction: NO TREMOR, p = 0.70
subject:  31 [epochs:   7] group: NO TREMOR, prediction: NO TREMOR, p = 0.99; adj. prediction: NO TREMOR, p = 0.89
subject:  32 [epochs:   7] group: NO TREMOR, prediction: NO TREMOR, p = 0.96; adj. prediction: NO TREMOR, p = 1.00
subject:  33 [epochs:   7] group: NO TREMOR, prediction: NO TREMOR, p = 0.99; adj. prediction: NO TREMOR, p = 1.00
subject:  34 [epochs:   8] group: NO TREMOR, prediction: NO TREMOR, p = 0.82; adj. prediction: NO TREMOR, p = 0.92
subject:  35 [epochs:   7] group: NO TREMOR, prediction: NO TREMOR, p = 0.92; adj. prediction: NO TREMOR, p = 1.00
subject:  36 [epochs:   7] group: NO TREMOR, prediction: NO TREMOR, p = 0.91; adj. prediction: NO TREMOR, p = 1.00
subject:  37 [epochs:   8] group: NO TREMOR, prediction: NO TREMOR, p = 1.00; adj. prediction: NO TREMOR, p = 1.00
subject:  38 [epochs:   7] group: NO TREMOR, prediction: NO TREMOR, p = 0.99; adj. prediction: NO TREMOR, p = 1.00
subject:  39 [epochs:   7] group: NO TREMOR, prediction: NO TREMOR, p = 0.97; adj. prediction: NO TREMOR, p = 1.00
subject:  40 [epochs:   8] group: NO TREMOR, prediction: NO TREMOR, p = 0.73; adj. prediction: NO TREMOR, p = 0.83
subject:  41 [epochs:   7] group: NO TREMOR, prediction: NO TREMOR, p = 0.80; adj. prediction: NO TREMOR, p = 1.00
subject:  42 [epochs:   7] group: NO TREMOR, prediction: NO TREMOR, p = 0.84; adj. prediction: NO TREMOR, p = 0.94
subject:  43 [epochs:   7] group: NO TREMOR, prediction:    TREMOR, p = 0.52; adj. prediction: NO TREMOR, p = 0.78
subject:  44 [epochs:   7] group: NO TREMOR, prediction: NO TREMOR, p = 0.55; adj. prediction: NO TREMOR, p = 0.85
subject:  45 [epochs:   7] group: NO TREMOR, prediction: NO TREMOR, p = 0.60; adj. prediction: NO TREMOR, p = 0.90
subject:  46 [epochs:   6] group:    TREMOR, prediction:    TREMOR, p = 0.95; adj. prediction:    TREMOR, p = 1.00
subject:  47 [epochs:   7] group:    TREMOR, prediction:    TREMOR, p = 0.93; adj. prediction:    TREMOR, p = 1.00
subject:  48 [epochs:   7] group:    TREMOR, prediction:    TREMOR, p = 0.94; adj. prediction:    TREMOR, p = 1.00
subject:  49 [epochs:   6] group:    TREMOR, prediction:    TREMOR, p = 0.94; adj. prediction:    TREMOR, p = 1.00
subject:  50 [epochs:   6] group:    TREMOR, prediction:    TREMOR, p = 0.92; adj. prediction:    TREMOR, p = 1.00
subject:  51 [epochs:   7] group:    TREMOR, prediction:    TREMOR, p = 0.93; adj. prediction:    TREMOR, p = 1.00
subject:  52 [epochs:   6] group:    TREMOR, prediction:    TREMOR, p = 0.91; adj. prediction:    TREMOR, p = 1.00
subject:  53 [epochs:   6] group:    TREMOR, prediction:    TREMOR, p = 0.92; adj. prediction:    TREMOR, p = 1.00
subject:  54 [epochs:   6] group:    TREMOR, prediction:    TREMOR, p = 0.89; adj. prediction:    TREMOR, p = 1.00
subject:  55 [epochs:   7] group:    TREMOR, prediction:    TREMOR, p = 0.79; adj. prediction:    TREMOR, p = 1.00
subject:  56 [epochs:   7] group:    TREMOR, prediction:    TREMOR, p = 0.75; adj. prediction:    TREMOR, p = 1.00
subject:  57 [epochs:   7] group:    TREMOR, prediction:    TREMOR, p = 0.90; adj. prediction:    TREMOR, p = 1.00
subject:  58 [epochs:   7] group:    TREMOR, prediction:    TREMOR, p = 0.90; adj. prediction:    TREMOR, p = 1.00
subject:  59 [epochs:   7] group:    TREMOR, prediction:    TREMOR, p = 0.88; adj. prediction:    TREMOR, p = 1.00
subject:  60 [epochs:   6] group:    TREMOR, prediction:    TREMOR, p = 0.94; adj. prediction:    TREMOR, p = 1.00
subject:  61 [epochs:   6] group:    TREMOR, prediction:    TREMOR, p = 0.92; adj. prediction:    TREMOR, p = 1.00
subject:  62 [epochs:   7] group:    TREMOR, prediction:    TREMOR, p = 0.93; adj. prediction:    TREMOR, p = 1.00
subject:  63 [epochs:   7] group:    TREMOR, prediction:    TREMOR, p = 0.95; adj. prediction:    TREMOR, p = 1.00
subject:  64 [epochs:   7] group:    TREMOR, prediction:    TREMOR, p = 0.91; adj. prediction:    TREMOR, p = 1.00
subject:  65 [epochs:   7] group:    TREMOR, prediction:    TREMOR, p = 0.95; adj. prediction:    TREMOR, p = 1.00
subject:  66 [epochs:   7] group:    TREMOR, prediction:    TREMOR, p = 0.95; adj. prediction:    TREMOR, p = 1.00
subject:  67 [epochs:   7] group:    TREMOR, prediction:    TREMOR, p = 0.95; adj. prediction:    TREMOR, p = 1.00
subject:  68 [epochs:   7] group:    TREMOR, prediction:    TREMOR, p = 0.94; adj. prediction:    TREMOR, p = 1.00
subject:  69 [epochs:   7] group:    TREMOR, prediction:    TREMOR, p = 0.91; adj. prediction:    TREMOR, p = 1.00
subject:  70 [epochs:   7] group:    TREMOR, prediction:    TREMOR, p = 0.91; adj. prediction:    TREMOR, p = 1.00
subject:  71 [epochs:   7] group:    TREMOR, prediction:    TREMOR, p = 0.87; adj. prediction:    TREMOR, p = 1.00
subject:  72 [epochs:   7] group:    TREMOR, prediction:    TREMOR, p = 0.92; adj. prediction:    TREMOR, p = 1.00
subject:  73 [epochs:   7] group:    TREMOR, prediction:    TREMOR, p = 0.74; adj. prediction:    TREMOR, p = 1.00
subject:  74 [epochs:   7] group:    TREMOR, prediction:    TREMOR, p = 0.88; adj. prediction:    TREMOR, p = 1.00
subject:  75 [epochs:   7] group:    TREMOR, prediction: NO TREMOR, p = 0.56; adj. prediction:    TREMOR, p = 0.74
subject:  76 [epochs:   7] group:    TREMOR, prediction: NO TREMOR, p = 0.61; adj. prediction:    TREMOR, p = 0.69
subject:  77 [epochs:   7] group:    TREMOR, prediction: NO TREMOR, p = 0.53; adj. prediction:    TREMOR, p = 0.77
subject:  78 [epochs:   7] group:    TREMOR, prediction:    TREMOR, p = 0.93; adj. prediction:    TREMOR, p = 1.00
subject:  79 [epochs:   7] group:    TREMOR, prediction:    TREMOR, p = 0.94; adj. prediction:    TREMOR, p = 1.00
subject:  80 [epochs:   7] group:    TREMOR, prediction:    TREMOR, p = 0.93; adj. prediction:    TREMOR, p = 1.00
subject:  81 [epochs:   7] group:    TREMOR, prediction:    TREMOR, p = 0.93; adj. prediction:    TREMOR, p = 1.00
subject:  82 [epochs:   7] group:    TREMOR, prediction:    TREMOR, p = 0.95; adj. prediction:    TREMOR, p = 1.00
subject:  83 [epochs:   7] group:    TREMOR, prediction:    TREMOR, p = 0.95; adj. prediction:    TREMOR, p = 1.00
subject:  84 [epochs:   7] group:    TREMOR, prediction:    TREMOR, p = 0.90; adj. prediction:    TREMOR, p = 1.00
subject:  85 [epochs:   6] group:    TREMOR, prediction:    TREMOR, p = 0.93; adj. prediction:    TREMOR, p = 1.00
subject:  86 [epochs:   7] group:    TREMOR, prediction:    TREMOR, p = 0.94; adj. prediction:    TREMOR, p = 1.00
subject:  87 [epochs:   7] group:    TREMOR, prediction:    TREMOR, p = 0.94; adj. prediction:    TREMOR, p = 1.00
subject:  88 [epochs:   7] group:    TREMOR, prediction:    TREMOR, p = 0.94; adj. prediction:    TREMOR, p = 1.00

RFC model accuracy report:
	log_loss: 0.15
	AUC: 0.9933
	misclassification rate: 0.05
	accuracy: 0.95
confusion matrix:
	sensitivity (TPR): 0.98
	specificity (TNR): 0.93
             ┌───────────────────┐
             │       Group       │
┌────────────┼─────────┬─────────┤
│ Prediction │no tremor│ tremor  │
├────────────┼─────────┼─────────┤
│ no tremor  │ 44      │ 3       │
├────────────┼─────────┼─────────┤
│   tremor   │ 1       │ 40      │
└────────────┴─────────┴─────────┘

adjusted prediction:
	sensitivity (TPR): 1.0
	specificity (TNR): 1.0
             ┌───────────────────┐
             │       Group       │
┌────────────┼─────────┬─────────┤
│ Prediction │no tremor│ tremor  │
├────────────┼─────────┼─────────┤
│ no tremor  │ 45      │ 0       │
├────────────┼─────────┼─────────┤
│   tremor   │ 0       │ 43      │
└────────────┴─────────┴─────────┘

Calculating predictions: first epoch only
subject:   1 group: NO TREMOR, prediction: NO TREMOR, p = 0.94; adj. prediction: NO TREMOR, p = 1.00
subject:   2 group: NO TREMOR, prediction: NO TREMOR, p = 0.96; adj. prediction: NO TREMOR, p = 1.00
subject:   3 group: NO TREMOR, prediction: NO TREMOR, p = 0.97; adj. prediction: NO TREMOR, p = 1.00
subject:   4 group: NO TREMOR, prediction: NO TREMOR, p = 0.84; adj. prediction: NO TREMOR, p = 1.00
subject:   5 group: NO TREMOR, prediction: NO TREMOR, p = 0.79; adj. prediction: NO TREMOR, p = 0.89
subject:   6 group: NO TREMOR, prediction: NO TREMOR, p = 0.62; adj. prediction: NO TREMOR, p = 0.72
subject:   7 group: NO TREMOR, prediction: NO TREMOR, p = 0.99; adj. prediction: NO TREMOR, p = 0.89
subject:   8 group: NO TREMOR, prediction: NO TREMOR, p = 0.95; adj. prediction: NO TREMOR, p = 0.65
subject:   9 group: NO TREMOR, prediction: NO TREMOR, p = 0.95; adj. prediction: NO TREMOR, p = 0.85
subject:  10 group: NO TREMOR, prediction: NO TREMOR, p = 1.00; adj. prediction: NO TREMOR, p = 0.70
subject:  11 group: NO TREMOR, prediction: NO TREMOR, p = 1.00; adj. prediction: NO TREMOR, p = 0.70
subject:  12 group: NO TREMOR, prediction: NO TREMOR, p = 0.98; adj. prediction: NO TREMOR, p = 0.68
subject:  13 group: NO TREMOR, prediction: NO TREMOR, p = 0.71; adj. prediction: NO TREMOR, p = 0.81
subject:  14 group: NO TREMOR, prediction: NO TREMOR, p = 0.90; adj. prediction: NO TREMOR, p = 0.80
subject:  15 group: NO TREMOR, prediction: NO TREMOR, p = 0.82; adj. prediction: NO TREMOR, p = 0.72
subject:  16 group: NO TREMOR, prediction: NO TREMOR, p = 0.66; adj. prediction: NO TREMOR, p = 0.56
subject:  17 group: NO TREMOR, prediction: NO TREMOR, p = 0.82; adj. prediction: NO TREMOR, p = 1.00
subject:  18 group: NO TREMOR, prediction: NO TREMOR, p = 0.63; adj. prediction: NO TREMOR, p = 0.53
subject:  19 group: NO TREMOR, prediction: NO TREMOR, p = 0.83; adj. prediction: NO TREMOR, p = 0.93
subject:  20 group: NO TREMOR, prediction: NO TREMOR, p = 0.87; adj. prediction: NO TREMOR, p = 1.00
subject:  21 group: NO TREMOR, prediction: NO TREMOR, p = 0.89; adj. prediction: NO TREMOR, p = 1.00
subject:  22 group: NO TREMOR, prediction: NO TREMOR, p = 0.92; adj. prediction: NO TREMOR, p = 0.62
subject:  23 group: NO TREMOR, prediction: NO TREMOR, p = 0.98; adj. prediction: NO TREMOR, p = 0.68
subject:  24 group: NO TREMOR, prediction: NO TREMOR, p = 0.99; adj. prediction: NO TREMOR, p = 1.00
subject:  25 group: NO TREMOR, prediction:    TREMOR, p = 0.66; adj. prediction:    TREMOR, p = 0.56
subject:  26 group: NO TREMOR, prediction: NO TREMOR, p = 0.99; adj. prediction: NO TREMOR, p = 0.69
subject:  27 group: NO TREMOR, prediction: NO TREMOR, p = 0.97; adj. prediction: NO TREMOR, p = 0.67
subject:  28 group: NO TREMOR, prediction: NO TREMOR, p = 1.00; adj. prediction: NO TREMOR, p = 0.70
subject:  29 group: NO TREMOR, prediction: NO TREMOR, p = 1.00; adj. prediction: NO TREMOR, p = 0.70
subject:  30 group: NO TREMOR, prediction: NO TREMOR, p = 1.00; adj. prediction: NO TREMOR, p = 0.70
subject:  31 group: NO TREMOR, prediction: NO TREMOR, p = 0.99; adj. prediction: NO TREMOR, p = 0.89
subject:  32 group: NO TREMOR, prediction: NO TREMOR, p = 0.98; adj. prediction: NO TREMOR, p = 1.00
subject:  33 group: NO TREMOR, prediction: NO TREMOR, p = 0.95; adj. prediction: NO TREMOR, p = 1.00
subject:  34 group: NO TREMOR, prediction: NO TREMOR, p = 0.84; adj. prediction: NO TREMOR, p = 0.94
subject:  35 group: NO TREMOR, prediction: NO TREMOR, p = 0.87; adj. prediction: NO TREMOR, p = 1.00
subject:  36 group: NO TREMOR, prediction: NO TREMOR, p = 0.95; adj. prediction: NO TREMOR, p = 1.00
subject:  37 group: NO TREMOR, prediction: NO TREMOR, p = 1.00; adj. prediction: NO TREMOR, p = 1.00
subject:  38 group: NO TREMOR, prediction: NO TREMOR, p = 0.98; adj. prediction: NO TREMOR, p = 1.00
subject:  39 group: NO TREMOR, prediction: NO TREMOR, p = 1.00; adj. prediction: NO TREMOR, p = 1.00
subject:  40 group: NO TREMOR, prediction: NO TREMOR, p = 0.74; adj. prediction: NO TREMOR, p = 0.84
subject:  41 group: NO TREMOR, prediction: NO TREMOR, p = 0.96; adj. prediction: NO TREMOR, p = 1.00
subject:  42 group: NO TREMOR, prediction:    TREMOR, p = 0.84; adj. prediction:    TREMOR, p = 0.74
subject:  43 group: NO TREMOR, prediction:    TREMOR, p = 0.52; adj. prediction: NO TREMOR, p = 0.58
subject:  44 group: NO TREMOR, prediction:    TREMOR, p = 0.53; adj. prediction: NO TREMOR, p = 0.77
subject:  45 group: NO TREMOR, prediction: NO TREMOR, p = 0.70; adj. prediction: NO TREMOR, p = 1.00
subject:  46 group:    TREMOR, prediction:    TREMOR, p = 0.96; adj. prediction:    TREMOR, p = 1.00
subject:  47 group:    TREMOR, prediction:    TREMOR, p = 0.92; adj. prediction:    TREMOR, p = 1.00
subject:  48 group:    TREMOR, prediction:    TREMOR, p = 0.92; adj. prediction:    TREMOR, p = 1.00
subject:  49 group:    TREMOR, prediction:    TREMOR, p = 0.95; adj. prediction:    TREMOR, p = 1.00
subject:  50 group:    TREMOR, prediction:    TREMOR, p = 0.92; adj. prediction:    TREMOR, p = 1.00
subject:  51 group:    TREMOR, prediction:    TREMOR, p = 0.94; adj. prediction:    TREMOR, p = 1.00
subject:  52 group:    TREMOR, prediction:    TREMOR, p = 0.91; adj. prediction:    TREMOR, p = 1.00
subject:  53 group:    TREMOR, prediction:    TREMOR, p = 0.91; adj. prediction:    TREMOR, p = 1.00
subject:  54 group:    TREMOR, prediction:    TREMOR, p = 0.88; adj. prediction:    TREMOR, p = 1.00
subject:  55 group:    TREMOR, prediction:    TREMOR, p = 0.88; adj. prediction:    TREMOR, p = 1.00
subject:  56 group:    TREMOR, prediction:    TREMOR, p = 0.86; adj. prediction:    TREMOR, p = 1.00
subject:  57 group:    TREMOR, prediction:    TREMOR, p = 0.90; adj. prediction:    TREMOR, p = 1.00
subject:  58 group:    TREMOR, prediction:    TREMOR, p = 0.92; adj. prediction:    TREMOR, p = 1.00
subject:  59 group:    TREMOR, prediction:    TREMOR, p = 0.88; adj. prediction:    TREMOR, p = 1.00
subject:  60 group:    TREMOR, prediction:    TREMOR, p = 0.92; adj. prediction:    TREMOR, p = 1.00
subject:  61 group:    TREMOR, prediction:    TREMOR, p = 0.92; adj. prediction:    TREMOR, p = 1.00
subject:  62 group:    TREMOR, prediction:    TREMOR, p = 0.94; adj. prediction:    TREMOR, p = 1.00
subject:  63 group:    TREMOR, prediction:    TREMOR, p = 0.94; adj. prediction:    TREMOR, p = 1.00
subject:  64 group:    TREMOR, prediction:    TREMOR, p = 0.96; adj. prediction:    TREMOR, p = 1.00
subject:  65 group:    TREMOR, prediction:    TREMOR, p = 0.96; adj. prediction:    TREMOR, p = 1.00
subject:  66 group:    TREMOR, prediction:    TREMOR, p = 0.95; adj. prediction:    TREMOR, p = 1.00
subject:  67 group:    TREMOR, prediction:    TREMOR, p = 0.96; adj. prediction:    TREMOR, p = 1.00
subject:  68 group:    TREMOR, prediction:    TREMOR, p = 0.94; adj. prediction:    TREMOR, p = 1.00
subject:  69 group:    TREMOR, prediction:    TREMOR, p = 0.90; adj. prediction:    TREMOR, p = 1.00
subject:  70 group:    TREMOR, prediction:    TREMOR, p = 0.92; adj. prediction:    TREMOR, p = 1.00
subject:  71 group:    TREMOR, prediction:    TREMOR, p = 0.86; adj. prediction:    TREMOR, p = 1.00
subject:  72 group:    TREMOR, prediction:    TREMOR, p = 0.92; adj. prediction:    TREMOR, p = 1.00
subject:  73 group:    TREMOR, prediction:    TREMOR, p = 0.78; adj. prediction:    TREMOR, p = 1.00
subject:  74 group:    TREMOR, prediction:    TREMOR, p = 0.85; adj. prediction:    TREMOR, p = 1.00
subject:  75 group:    TREMOR, prediction:    TREMOR, p = 0.53; adj. prediction:    TREMOR, p = 0.83
subject:  76 group:    TREMOR, prediction:    TREMOR, p = 0.59; adj. prediction:    TREMOR, p = 0.89
subject:  77 group:    TREMOR, prediction:    TREMOR, p = 0.57; adj. prediction:    TREMOR, p = 0.87
subject:  78 group:    TREMOR, prediction:    TREMOR, p = 0.96; adj. prediction:    TREMOR, p = 1.00
subject:  79 group:    TREMOR, prediction:    TREMOR, p = 0.95; adj. prediction:    TREMOR, p = 1.00
subject:  80 group:    TREMOR, prediction:    TREMOR, p = 0.95; adj. prediction:    TREMOR, p = 1.00
subject:  81 group:    TREMOR, prediction:    TREMOR, p = 0.90; adj. prediction:    TREMOR, p = 1.00
subject:  82 group:    TREMOR, prediction:    TREMOR, p = 0.96; adj. prediction:    TREMOR, p = 1.00
subject:  83 group:    TREMOR, prediction:    TREMOR, p = 0.95; adj. prediction:    TREMOR, p = 1.00
subject:  84 group:    TREMOR, prediction:    TREMOR, p = 0.74; adj. prediction:    TREMOR, p = 1.00
subject:  85 group:    TREMOR, prediction:    TREMOR, p = 0.93; adj. prediction:    TREMOR, p = 1.00
subject:  86 group:    TREMOR, prediction:    TREMOR, p = 0.94; adj. prediction:    TREMOR, p = 1.00
subject:  87 group:    TREMOR, prediction:    TREMOR, p = 0.94; adj. prediction:    TREMOR, p = 1.00
subject:  88 group:    TREMOR, prediction:    TREMOR, p = 0.95; adj. prediction:    TREMOR, p = 1.00

RFC model accuracy report:
	log_loss: 0.1625
	AUC: 0.9956
	misclassification rate: 0.05
	accuracy: 0.95
confusion matrix:
	sensitivity (TPR): 0.91
	specificity (TNR): 1.0
             ┌───────────────────┐
             │       Group       │
┌────────────┼─────────┬─────────┤
│ Prediction │no tremor│ tremor  │
├────────────┼─────────┼─────────┤
│ no tremor  │ 41      │ 0       │
├────────────┼─────────┼─────────┤
│   tremor   │ 4       │ 43      │
└────────────┴─────────┴─────────┘

adjusted prediction:
	sensitivity (TPR): 1.0
	specificity (TNR): 1.0
             ┌───────────────────┐
             │       Group       │
┌────────────┼─────────┬─────────┤
│ Prediction │no tremor│ tremor  │
├────────────┼─────────┼─────────┤
│ no tremor  │ 45      │ 0       │
├────────────┼─────────┼─────────┤
│   tremor   │ 0       │ 43      │
└────────────┴─────────┴─────────┘
